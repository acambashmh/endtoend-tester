﻿var validatorFactory = require('./validatorFactory.js');
var ruleFactory = require('./ruleFactory.js');
var Validator = require('jsonschema').Validator;
var v = new Validator();

function getAssignmentValidator() {
    var rules = [];
    rules.push(createStructureRule());
    var validator = new validatorFactory(rules, 'Assignment validator');
    return validator;
}

function createStructureRule() {
    var testSchema = {
        "id": "assignmentSchema",
        "title" : "Assignment schema",
        "type": "array",
        "items": { "$ref": "#/definitions/assignment" },
        "definitions" : {
            "assignment" : {
                "type": "object",
                "properties": {
                    "refId": { "type": "string", "required": true },
                    "title": { "type": "string", "required": true },
                    "preamble": { "type": "string", "required": true },
                    "staffPersonalRefId": { "type": "string", "required": true },
                    "creatorId": { "type": "string", "required": true },
                    "status": { "type": "string", "required": true },
                    "platformId": { "type": "string", "required": true },
                    "platformAssignmentId": {"type": ["string","null"]},
                    "sectionId": { "type": "string", "required": true },
                    "students": { "type": "array" },
                    "sourceObjects": {
                        "type": "array",
                        "items" : {
                            "$ref" : "#/definitions/assignment/properties/sourceObjects/definitions/sourceObject"
                        },
                        "definitions" : {
                            "sourceObject" : {
                                "type": "object",
                                "properties": {
                                    "value": { "type": "string", "required": true }, 
                                    "sif_RefObject": { "type": "string", "required": true }, 
                                    "isbn": { "type": "string", "required": true }
                                }
                            
                            }
                        }
                    },
                    "availableDate": { "type": "string" },
                    "dueDate": { "type": "string" }
                }
            }
        }
    }

    function ruleFunc(data) {
        var result = {
            valid: true,
        };

        var res = v.validate(data, testSchema);
        if (res.errors.length > 0) {
            result.valid = false;
            result.message = res.errors.toString();
            return result;
        }
        return true;
    }
    
    var rule = new ruleFactory(ruleFunc, 'schema of Assignment');
    return rule;
}


exports.getAssignmentValidator = getAssignmentValidator;