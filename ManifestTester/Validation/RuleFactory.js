﻿
function Rule(checkRuleFunc, name) {
    var self = this;
    self.name = name || 'no name';
    
    function createResult(valid, message) {
        var result = {
            valid : valid,
            message: message
        };
    }

    self.checkRule = function (objToValidate) {
        var result = {
            valid : false,
            message: "not yet run"
        };

        try {
            var ruleCheckResult = checkRuleFunc(objToValidate);
            //If the rule func is simple true or false
            if (ruleCheckResult === true || ruleCheckResult === false) {
                if (ruleCheckResult) {
                    result = new RuleResult(ruleCheckResult, self.name + ' passed');
                } else {
                    result = new RuleResult(ruleCheckResult, self.name + ' failed');
                }
            } 
            //if the rule func returns an object 
            else if (typeof ruleCheckResult === 'object' && ruleCheckResult.message) {
                result = new RuleResult(ruleCheckResult.valid, ruleCheckResult.message);
            }
            else { 
                result = new RuleResult(false, 'rule failed for unknown reasons');
            }

        } catch (e) { 
            result = new RuleResult(false, 'rule failed: ' + e.toString())
        }
        
        return result;
    };
}

function RuleResult(valid, message) {
    var self = this;
    self.valid = valid;
    self.message = message;
}

module.exports = Rule;