﻿var validatorFactory = require('./validatorFactory.js');
var ruleFactory = require('./ruleFactory.js');
var Validator = require('jsonschema').Validator;
var _ = require('underscore');

var v = new Validator();

function getTocValidator() {
    var rules = [];
    rules.push(createStructureRule());
    rules.push(createGradeRule());
    var validator = new validatorFactory(rules, 'toc validator');
    return validator;
}



function createStructureRule() {
    var tocSchema = {
        "id": "tocSchema",
        "title": "toc schema",
        "type": "array",
        "items" : {
            "type": "object",
            "properties": {
                "level_number": { "type": "string" },
                "nonGradeLevel": { "type": "string" },
                "nonGradeTitle": { "type": "string" },
                "number": { "type": "string" },
                "level" : {
                    "type": "array",
                    "minItems": 1,
                    "required": true,
                    "items": {
                        "$ref": "#/definitions/levelItemProperties"
                    }
                }
            }
        },
        "definitions" : {
            "levelItemProperties" : {
                "type": "object",
                "properties": {
                    "hierarchy": { "type": "string", "required": true },
                    "type": { "type": "string", "required": true },
                    "level_number": { "type": "string" },
                    "title": { "type": "string", "required": true },
                    "number": { "type": "string" },
                    "resources": {
                        "$ref" : "#/definitions/resourcesItem"
                    },
                    "level" : {
                        "type": "array",
                        "items" : {
                            "$ref": "#/definitions/levelItemProperties"
                        }
                    }
                }
            },
            "resourcesItem" : {
                "type" : "object",
                "properties" : {
                    "resource" : {
                        "type": "array",
                        "items" : {
                            "type" : "object",
                            "properties" : {
                                "HMH_ID": { "type": "string", "required": true },
                                "id": { "type": "string" },
                                "display_title": { "type": "string", "required": true },
                                "tool_type": { "type": "number", "required": true },
                                "assignable": { "type": "boolean", "required": true },
                                "component_type": { "type": "string", "required": true },
                                "media_type": { "type": "string", "required": true },
                                "categorization": { "type": "string", "required": true },
                                "uri": {
                                    "type": "object", 
                                    "properties": {
                                        "relative_url": { "type": "string", "required": true }, 
                                        "platform_url": { "type": "string", "required": true }
                                    }
                                },
                                "ISBN": {
                                    "type": "array", 
                                    "items" : {
                                        "type" : "object",
                                        "properties": {
                                            "type": { "type": "string", "required": true }, 
                                            "value": { "type": "string", "required": true }
                                        }
                                    }
                                },
                            }
                        }
                    }
                }
            }
        }
    };
    
    var rule = new ruleFactory(function (tocData) {
        var result = {
            valid: true,
        };
        
        try {
            var schemaResult = v.validate(tocData.levels.level, tocSchema);
            
            if (schemaResult.errors.length > 0) {
                result.valid = false;
                result.message = schemaResult.errors.toString();
                return result;
            }
        } catch (error) {
            return false;
        }
        
        return true;
    }, 'structure of toc and check for data types');
    return rule;
}

function createGradeRule() {
    var rule = new ruleFactory(function (tocData) {
        var result = {
            valid: true,
            message: 'all ok'
        };
        var units = tocData.levels.level[0].level;
        try {
            var chapOrModuleType = tocData.levels.level[0].level[0].level[0].type;
            for (var i = 0; i < units.length ; i++) {
                if (units[i].type !== 'unit') { 
                    throw new Error('unit type error');
                }
                var chapOrModuleList = units[i].level;
                for (var j = 0; j < chapOrModuleList.length; j++) {
                    if (chapOrModuleList[0].type !== chapOrModuleType) { 
                        throw new Error('chapter/module type error: should be '+ chapOrModuleList[0].type + ' and is ' + chapOrModuleType);
                    }
                    var unitList = chapOrModuleList[j].level;
                    for (var z = 0; z < unitList.length; z++) { 
                        if (unitList[0].type !== 'lesson') {
                            throw new Error('chapter/module type error: should be lesson and is ' + unitList[0].type);
                        }
                    }
                }
            }
        } catch (e) {
            result = {
                valid: false,
                message: e.toString()
            }
        }
        return result;
    }, 'module/chapter rule');

    return rule;
}


exports.getTocValidator = getTocValidator;