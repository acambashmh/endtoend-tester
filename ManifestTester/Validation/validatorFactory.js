﻿var rulesFactory = require('./ruleFactory');

function Validator( arrayOfRules, name) {
    var self = this;
    self.rules = arrayOfRules;
    self.name = name || 'no name';
    
    this.validate = function (validationObject) {
        var result = new ValidationResult(true, []);
        
        var someRuleFailed = false;

        for (var i = 0; i < self.rules.length; i++) {
            var ruleResult = self.rules[i].checkRule(validationObject);

            if (!ruleResult.valid) {
                result.valid = false;
                result.messages.push(self.rules[i].name + ' - ' + ruleResult.message); 
            }
        }
        
        if (result.valid) {
            result.messages.push('data is valid');
        }

        return result;
    }
}

function ValidationResult(valid, messages) {
    var self = this;

    self.messages = messages || [];
    self.valid = valid;
}

module.exports = Validator;