﻿var xml2js = require('xml2js');
var q = require('q');
var _ = require('underscore');
var fs = require('fs');
var xlsx = require('xlsx');

//main method
var validateTocManifestStandars = function (manifestList, standardsRaw) {
    var result = [];
    var parser = new xml2js.Parser();
    return q.nfcall(parser.parseString, standardsRaw)
    .then(function (data) {
        var refactorStandardData = [];
        if (data.standard_set) {
            refactorStandardData = refactorNationalStandards(data.standard_set);
        } else if (data.correlationResponse) {
            refactorStandardData = refactorCaliforniaStandards(data.correlationResponse);
        }
        
        _.each(manifestList, function (manifest) {
            var standardsTab = _.findWhere(manifest.data.navigation_menu.menu_tabs, { tab_description : "Standards" })
        });
        
        return result;
    });
}

//main method
var validateManifestSheetStandards = function (mdsSheetPath, standardsPath) {
    var result = [];
    var getStandardsDataPromise = getStandardsDataFromStandardXML(standardsPath);
    var getManifestDataPromise = getManifestDataFromSheet(mdsSheetPath);
    
    return q.all([getManifestDataPromise, getStandardsDataPromise])
    .then(function (data) {
        var validateResult = validateManifestAndStandardsData(data[0], data[1]);
        return validateResult;
    })
}

//Validates refactored data
var validateManifestAndStandardsData = function (manifestItems, standardItems) {
    var result = [];
    
    _.each(manifestItems, function (manifestItem) {
        var missingStandardCodesString = '';
        var standardsError = false;
        _.each(manifestItem.standardCodes, function (manifestStandardCode) {
            //!!!
            var tempCode = _.findWhere(standardItems, { code: manifestStandardCode });
            if (!tempCode) {
                standardsError = true;
                missingStandardCodesString = missingStandardCodesString + ' ' + manifestStandardCode + ' , ';
            }
        })
        if (standardsError) {
            manifestItem.error = 'The missing standard codes are: ' + missingStandardCodesString;
            result.push(manifestItem);
        }

    });
    
    return result;
}

//refactors data from standard xml
var getStandardsDataFromStandardXML = function (standardsPath) {
    var parser = new xml2js.Parser();
    var a = q.nfcall(fs.readFile, standardsPath, 'utf-8')
    .then(function (xmlString) {
        return q.nfcall(parser.parseString, xmlString)
    })
    .then(function (xmlData) {
        var refactorStandardData = [];
        if (xmlData.standard_set) {
            refactorStandardData = refactorNationalStandards(xmlData.standard_set);
        } else if (xmlData.correlationResponse) {
            refactorStandardData = refactorCaliforniaStandards(xmlData.correlationResponse);
        }
        
        var unique = [];
        var groupedByResource = _.groupBy(refactorStandardData, 'code');
        for (var key in groupedByResource) {
            unique.push(groupedByResource[key][0]);
        }
        console.log(unique.length);
        return unique;
    });
    
    return a;
}

//refactors data from mds spreadsheet
var getManifestDataFromSheet = function (mdsSheetPath) {
    var defer = q.defer();
    promise = defer.promise;
    
    var workbook = xlsx.readFile(mdsSheetPath);
    var sheetName = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[sheetName];
    var startRow = 5;
    var numOfRows = +worksheet['!ref'].split(':')[1].replace(new RegExp('[A-Z]+'), '');
    var rows = new Array();
    for (var i = startRow; i < numOfRows; i++) {
        var standardsString = (worksheet['AZ' + i]) ? worksheet['AZ' + i].v : "";
        if (standardsString !== '') {
            var id = (worksheet['AY' + i]) ? worksheet['AY' + i].v : "";
            var guid = (worksheet['AM' + i]) ? worksheet['AM' + i].v : "";
            var displayTitle = (worksheet['AL' + i]) ? worksheet['AL' + i].v : "";
            var standards = standardsString.replace(/\s/g, "").split(',');
            rows.push(new manifestItem(id, guid, displayTitle, standards));
        }
    }
    defer.resolve(rows);
    
    return promise;
}


//ZORAN TODO :)

//gets data from california standards xml
function refactorCaliforniaStandards(data) {
    var result = new Array();
    var correlationElement = data.correlationElement;
    _.each(data.correlationElement, function (correlationElement) {
        var standardCode = null;
        if (correlationElement.corrLeftSide[0].standardDataElem[0].customCodes[0] !== '' &&
            correlationElement.corrLeftSide[0].standardDataElem[0].customCodes[0].customCode[0].$.customCodeValue) {
            standardCode = correlationElement.corrLeftSide[0].standardDataElem[0].customCodes[0].customCode[0].$.customCodeValue;
        } 
        else {
            standardCode = correlationElement.corrLeftSide[0].standardDataElem[0].stateNumber[0];
        }
        var standardDescription = correlationElement.corrLeftSide[0].standardDataElem[0].title[0];
        result.push(new standardItem(standardCode, standardDescription));
    });
    return result;
}

function _getStandardsForNA(standard, result) {
    var standardDescription = "";
    var standardCode = "";
    for (var i = 0, len = standard.length; i < len; i++) {
        standardDescription = standard[i].description[0]['xhtml:div'][0];
        standardCode = standard[i].codes[0].$.official.trim();
        if (standard[i].standard) {
            _getStandardsForNA(standard[i].standard, result);
        }
        if(standardCode != '')
            result.push(new standardItem(standardCode, standardDescription))
    }
};

//gets data from national standards xml
function refactorNationalStandards(data) {
    var result = new Array();
    _getStandardsForNA(data.standard, result);
    return result;
}

//class for data extracted from standards xml
function standardItem(code, description) {
    var self = this;
    self.code = code;
    self.description = description;
}

//class for data extracted from mds sheet
function manifestItem(id, guid, displayTitle, standardCodes) {
    var self = this;
    self.id = id;
    self.guid = guid;
    self.displayTitle = displayTitle;
    self.standardCodes = standardCodes;
}

//Exports
exports.validateTocManifestStandars = validateTocManifestStandars;
exports.validateManifestSheetStandards = validateManifestSheetStandards;