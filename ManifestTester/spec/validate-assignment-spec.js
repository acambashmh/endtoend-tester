﻿var q = require('q');
var assignmentValidationFactory = require('../Validation/assignmentValidator');
var assignmentService = require('../services/assignment');
var identityFactory = require('../services/identity');
var config = require('../Infrastructure/manifestTesterConfig');
//Test data
var assignmentValidator = assignmentValidationFactory.getAssignmentValidator();
var identity = new identityFactory();
var defaultAssignmentData = config.getConfigDataSync().assignmentDefaultValues;

//Unit tests
describe('check data', function () {
    
    it('should validate assignment on int', function () {
        var result = false
        var waitStarted = false;
        var waitDone = false;
        
        waitsFor(function () {
            if (!waitStarted) {
                waitStarted = true;
                var HostData = config.getHostDataForPlatform('int');
                
                identity.getIdentityData(HostData.username, HostData.password, HostData.name)
                    .then(function (data) {
                    return assignmentService.getAssignments(data.access_token, HostData.name, defaultAssignmentData.sectionId)
                            .then(unitTestHandler);
                });
                
                function unitTestHandler(data) {
                    waitDone = true;
                    var assignmentValidator = assignmentValidationFactory.getAssignmentValidator();
                    result = assignmentValidator.validate(data);

                }
            }
            return waitDone;
        }, 'Time is up for async function', 12000);
        
        runs(function () {
            if (!result.valid) {
                result.message
            }
            expect(result.valid).toBe(true);
        });
    });
    

    it('should validate assignment on cert', function () {
        var result = false
        var waitStarted = false;
        var waitDone = false;
        
        waitsFor(function () {
            if (!waitStarted) {
                waitStarted = true;
                var HostData = config.getHostDataForPlatform('cert');
                
                identity.getIdentityData(HostData.username, HostData.password, HostData.name)
                    .then(function (data) {
                    return assignmentService.getAssignments(data.access_token, HostData.name, defaultAssignmentData.sectionId)
                            .then(unitTestHandler);
                });
                
                function unitTestHandler(data) {
                    waitDone = true;
                    var assignmentValidator = assignmentValidationFactory.getAssignmentValidator();
                    result = assignmentValidator.validate(data);
                }
            }
            return waitDone;
        }, 'Time is up for async function', 12000);
        
        runs(function () {
            if (!result.valid) {
                result.message
            }
            expect(result.valid).toBe(true);
        });
    });


    it('should validate assignment on cert-review', function () {
        var result = false
        var waitStarted = false;
        var waitDone = false;
        
        waitsFor(function () {
            if (!waitStarted) {
                waitStarted = true;
                
                var HostData = config.getHostDataForPlatform('int');
                
                identity.getIdentityData(HostData.username, HostData.password, HostData.name)
                    .then(function (data) {
                    return assignmentService.getAssignments(data.access_token, HostData.name, defaultAssignmentData.sectionId)
                            .then(unitTestHandler);
                });
                
                function unitTestHandler(data) {
                    waitDone = true;
                    var assignmentValidator = assignmentValidationFactory.getAssignmentValidator();
                    result = assignmentValidator.validate(data);

                }
            }
            return waitDone;
        }, 'Time is up for async function', 12000);
        
        runs(function () {
            if (!result.valid) {
                result.message
            }
            expect(result.valid).toBe(true);
        });
    });

});