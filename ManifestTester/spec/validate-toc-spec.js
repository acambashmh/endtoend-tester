﻿var q = require('q');
var tocValidationFactory = require('../Validation/tocValidator');
var tocService = require('../services/toc');
var config = require('../Infrastructure/manifestTesterConfig');
var identityFactory = require('../services/identity');

//Test data
var tocValidator = tocValidationFactory.getTocValidator();
var tocDefaultData = config.getConfigDataSync().tocDefaultValues;
var identity = new identityFactory();

//Unit tests
describe('check toc', function () {
    
    it('should validate toc on cert', function () {
        var result = false
        var waitStarted = false;
        var waitDone = false;
        
        waitsFor(function () {
            if (!waitStarted) {
                waitStarted = true;
                var hostData = config.getHostDataForPlatform('cert');

                identity.getIdentityData(hostData.username, hostData.password, hostData.name)
                .then(function (data) {
                    new tocService.getTOC(data.access_token, hostData.name, tocDefaultData.program, tocDefaultData.grade, 
                        tocDefaultData.non_grade_level, tocDefaultData.non_grade_title, tocDefaultData.version, tocDefaultData.showResources,
                        tocDefaultData.outputFormat).then(unitTestHandler)
                });

                function unitTestHandler(data) {
                    waitDone = true;
                    result = tocValidator.validate(data)
                }
            }
            return waitDone;
        }, 'Time is up for async function', 25000);
        
        runs(function () {
            if (!result.valid) {
                for (var i = 0; i < result.messages.length; i++) {
                    console.log(result.messages[i]);
                }
            }
            expect(result.valid).toBe(true);
        });
    });
    
    it('should validate toc on int', function () {
        var result = false
        var waitStarted = false;
        var waitDone = false;
        
        waitsFor(function () {
            if (!waitStarted) {
                waitStarted = true;
                var hostData = config.getHostDataForPlatform('int');
                
                identity.getIdentityData(hostData.username, hostData.password, hostData.name)
                .then(function (data) {
                    tocService.getTOC(data.access_token, hostData.name, tocDefaultData.program, tocDefaultData.grade, 
                        tocDefaultData.non_grade_level, tocDefaultData.non_grade_title, tocDefaultData.version, tocDefaultData.showResources,
                        tocDefaultData.outputFormat).then(unitTestHandler)
                });
                
                function unitTestHandler(data) {
                    waitDone = true;
                    result = tocValidator.validate(data)
                }
            }
            return waitDone;
        }, 'Time is up for async function', 25000);
        
        runs(function () {
            if (!result.valid) { 
                for (var i = 0; i < result.messages.length; i++) {
                    console.log(result.messages[i]);
                }
            }
            expect(result.valid).toBe(true);
        });
    });
    
   it('should validate toc on cert-review', function () {
        var result = false
        var waitStarted = false;
        var waitDone = false;
        
        waitsFor(function () {
            if (!waitStarted) {
                waitStarted = true;
                var hostData = config.getHostDataForPlatform('cert-review');

                identity.getIdentityData(hostData.username, hostData.password, hostData.name)
                .then(function (data) {
                    tocService.getTOC(data.access_token, hostData.name, tocDefaultData.program, tocDefaultData.grade, 
                        tocDefaultData.non_grade_level, tocDefaultData.non_grade_title, tocDefaultData.version, tocDefaultData.showResources,
                        tocDefaultData.outputFormat).then(unitTestHandler)
                });

                function unitTestHandler(data) {
                    console.log(data);
                    waitDone = true;
                    result = tocValidator.validate(data)
                }
            }
            return waitDone;
        }, 'Time is up for async function', 25000);
        
        runs(function () {
            if (!result.valid) {
                for (var i = 0; i < result.messages.length; i++) {
                    console.log(result.messages[i]);
                }
            }
            expect(result.valid).toBe(true);
        });
    });

});