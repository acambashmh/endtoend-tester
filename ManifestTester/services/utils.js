﻿var Utils = function () {
    this.errorHandler = function (code) {
        switch (code) {
            case 500:
                throw new Error('500: Internal server error');
            case 503:
                throw new Error('503: Service Unavailable');
            case 404:
                throw new Error('404: Not found error');
            case 403:
                throw new Error('403: Authentication error');
            case 401:
                throw new Error('401: Authentication error');
            case 400:
                throw new Error('400: Bad Request error');
            default:
                return;
        }
    }
}

module.exports = new Utils();