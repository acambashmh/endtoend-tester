﻿var http = require('q-io/http');
var utils = require('./utils.js');
var config = require('../Infrastructure/manifestTesterConfig');

var Sections = function () {

    
    var processResponse = function (response) {
        var responseData = "";
        utils.errorHandler(response.status);
        return response.body.forEach(function (data) {
            responseData += data;
        }).then(function () {
            return JSON.parse(responseData);
        });
    };
    
    this.getSections = function (token, environment, refId, platform, isStudent) {
        var defPath = '/api/identity/v1/staffPersons/staffPerson/';
        if (isStudent === true || isStudent === 'true') { 
            defPath =  '/api/identity/v1/students/student/'
        }

        var options = {
            hostname: config.getHostDataForPlatform().hostUrl,
            path: defPath,
            method: 'GET',
            headers: {
                'Authorization': ''
            }
        };
        

        options.path = defPath + refId + "/section;contextId="+platform;
        options.hostname = config.getHostDataForPlatform(environment).hostUrl;
        options.headers = { 'Authorization': token};
        return http.request(options).then(processResponse);
    };
}

module.exports = new Sections();