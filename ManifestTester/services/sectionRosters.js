﻿var identity = require('./identity.js');
var http = require('q-io/http');
var utils = require('./utils.js');
var config = require('../Infrastructure/manifestTesterConfig');

var SectionRosters = function () {
 
    var processResponse = function (response) {
        var responseData = "";
        utils.errorHandler(response.status);
        return response.body.forEach(function (data) {
            responseData += data;
        })
        .then(function () {
            return JSON.parse(responseData);
        });
    };
    
    var defPath = '/api/identity/v1/sectionRosters/';
    
    var options = {
        hostname: '',
        path: defPath,
        headers: {
            'Authorization': ''
        }
    };
    
    
    this.getSectionRosters = function (token, environment, refId, platform) {

        options.hostname = config.getHostDataForPlatform(environment).hostUrl;
        options.path = defPath + refId+';contextId='+platform;
        options.headers.Authorization = token;
        
        return http.request(options).then(processResponse);
    }

}

module.exports =  SectionRosters;