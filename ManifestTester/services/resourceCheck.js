﻿var q = require('q');
var _ = require('underscore');
var request = require('request');
var config = require('../Infrastructure/manifestTesterConfig');
var standardsValidation = require('../Validation/standardsValidation');

function ResourceItem(HMH_ID, title, resourceUri, type) {
    var self = this;
    self.HMH_ID = HMH_ID;
    self.title = title;
    self.resourceUri = resourceUri;
    self.type = type;
}

var getResourceUrl = function (resourceUrl, hostData, platform) {
    var url = '';
    if (resourceUrl.indexOf("http") !== -1) {
        url = resourceUrl;
    } else {
        if (platform === 'tc') {
            url = hostData.resourceUrlTc + resourceUrl;
        } else {
            url = hostData.resourceUrl + resourceUrl;
        }
    }
    return url;
}

var getLevelResources = function (level) {
    var resources = [];
    for (var i = 0; i < level.length; i++) {
        var levelItem = level[i];
        if (levelItem.level) {
            var res = getLevelResources(levelItem.level);
            resources = _.union(resources, res);
        }
        if (levelItem.resources && levelItem.resources.resource) {
            _.each(levelItem.resources.resource, function (levelResource) {
                if (levelResource.download_url.indexOf('manifest') !== -1) {
                    var manifestResourceItem = new ResourceItem(levelResource.HMH_ID, 'download manifest', 
                        levelResource.download_url, levelResource.media_type);
                    resources.push(manifestResourceItem);
                }
                
                var levelResourceItem = new ResourceItem(levelResource.HMH_ID, levelResource.display_title, 
                    levelResource.uri.relative_url, levelResource.media_type);
                resources.push(levelResourceItem);
            });
        }
    }
    return resources;
}

var makeResourceListFromToc = function (tocData) {
    var result = getLevelResources(tocData.levels.level[0].level);
    var groupedByResource = _.groupBy(result, 'resourceUri');
    var unique = [];
    for (var key in groupedByResource) {
        unique.push(groupedByResource[key][0]);
    }
    return unique;
};


var getResourcesFromManifests = function (manifestList) {
    var resources = [];
    var unique = [];
    if (manifestList) { 
        _.each(manifestList, function (manifest) {
            if (manifest.valid) {
                _.each(manifest.data.download_packages.downloads, function (downloadPackage) {
                    if (downloadPackage.package_details.download_uri && downloadPackage.type !== 'pmt') {
                        var resource = new ResourceItem(manifest.id, downloadPackage.description, 
                                            downloadPackage.package_details.download_uri.relative_url,
                                            downloadPackage.type);
                        resources.push(resource);
                    }
                });
            }
        });
        var groupedByResource = _.groupBy(resources, 'resourceUri');
        for (var key in groupedByResource) {
            unique.push(groupedByResource[key][0]);
        }
    }
    return unique;
}

var getManifestsFromResources = function (resourceList, hostData, platform) {
    var promiseGetManifest = [];
    var manifestResourceList = _.where(resourceList, { title: 'download manifest' });
    
    if (manifestResourceList) {
        _.each(manifestResourceList, function (manifestResourceItem) {
            (function () {
                var defer = q.defer();
                var promise = defer.promise;
                var url = getResourceUrl(manifestResourceItem.resourceUri, hostData, platform);
                request.get(url, function (error, response, body) {
                    if (error || response.statusCode !== 200) {
                        defer.resolve({
                            error: error,
                            valid: false
                        });
                    } else {
                        defer.resolve({
                            data: JSON.parse(body),
                            id: manifestResourceItem.HMH_ID,
                            valid: true
                        });
                    }
                });
                promiseGetManifest.push(promise);
            }());
        });
    }
    return q.all(promiseGetManifest)
}

var checkAllResources = function (resourceList, hostData, platform) {
    //resourceList = _.first(resourceList, 100);
    var listLength = resourceList.length;
    var startTime = new Date();
    var endTime = new Date();
    var promiseList = [];
    var errorResources = [];
    var i = 0;
    
    if (resourceList) {
        _.each(resourceList, function (resourceItem) {
            (function () {
                var url = getResourceUrl(resourceItem.resourceUri, hostData, platform);
                var deferred = q.defer();
                var promise = deferred.promise;
                request.head(url, function (error, response, body) {
                    i = i + 1;
                    console.log('done with ' + i + 'out of ' + listLength);
                    if (error || response.statusCode !== 200) {
                        errorResources.push(resourceItem);
                    }
                    deferred.resolve();
                });
                promiseList.push(promise);
            }());
        });
    }
    return q.all(promiseList).then(function (data) {
        endTime = new Date();
        var result = {
            startTime : startTime,
            endTime : endTime,
            errorResources : errorResources,
        };
        return result;
    });
};


var checkAllTocResources = function (toc, environment, platform) {
    //get host url
    var hostData = config.getHostDataForPlatform(environment);
    //get resources from toc
    var tocResources = makeResourceListFromToc(toc);
    //get manifests 
    var returnPromise = getManifestsFromResources(tocResources, hostData, platform)
    .then(function (data) {
        //get resources from manifests
        var manifestResources = getResourcesFromManifests(data);
        var allResources = _.union(tocResources, manifestResources);
        return allResources;
    })
    .then(function (data) {
        //check all resources
        //return data.length;
        return checkAllResources(data, hostData, platform);
    })
    return returnPromise;
}

var checkStandardsInToc = function (toc, environment, platform, standardsRaw) {
    //get host url
    var hostData = config.getHostDataForPlatform(environment);
    //get resources from toc
    var tocResources = makeResourceListFromToc(toc);
    var mainPromise = getManifestsFromResources(tocResources, hostData, platform)
    .then(function (data) {
        var manifests = _.where(data, { valid: true });
        return standardsValidation.validateTocManifestStandars(manifests, standardsRaw);
    })
    .then(function (data) { 
        return data
    });

    return mainPromise;
}


exports.checkStandardsInToc = checkStandardsInToc;

exports.checkAllTocResources = checkAllTocResources;