﻿var identity = require('./identity.js');
var http = require('q-io/http');
var utils = require('./utils.js');
var config = require('../Infrastructure/manifestTesterConfig');

var Assignemnt = function () {
    
    var processResponse = function (response) {
        var responseData = "";
        utils.errorHandler(response.status);
        
        return response.body.forEach(function (data) {
            responseData += data;
        }).then(function () {
                return JSON.parse(responseData);
            });
    };
    
    this.getAssignments = function (token, environment, sectionId) {
        var options = new makeOptions(config.getHostDataForPlatform(environment).hostUrl, sectionId, token);
        return http.request(options).then(processResponse);
    };
}

function makeOptions(hostUrl, sectionId, token) {
    var defPath = '/api/assignment/v1/teacherAssignments?sectionId=';
    var self = this;
    self.hostname = hostUrl;
    self.path = defPath + sectionId;
    self.headers = {};
    self.headers.Authorization = token;
    self.method = 'GET';
}

module.exports = new Assignemnt();