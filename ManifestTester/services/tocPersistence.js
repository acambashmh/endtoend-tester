﻿var Q = require('q');
var datastore = require('nedb');

function TocPesristence() {
    var self = this;
    
    self.insertToc = function ( toc, environment, program, grade, nonGradeLevel, nonGradeTitile) {
        var deferred = Q.defer();
        var tocDO = new TocDataObject(toc, environment, program, grade, nonGradeLevel, nonGradeTitile);
        self.db.insert(tocDO, function (err, newDoc) {
            deferred.resolve(true);
        });
        return deferred.promise;
    }

    self.getTocList = function () {
        var deferred = Q.defer();
        self.db.find({}, {toc:0}).sort({ environment : -1, date: -1 }).exec(function (err, newDoc) {
            deferred.resolve(newDoc);
        });
        return deferred.promise;
    }
    
    self.getTocById = function (id) {
        var deferred = Q.defer();
        self.db.findOne({_id:id}).exec(function (err, newDoc) {
            deferred.resolve(newDoc);
        });
        return deferred.promise;
    }
    
    self.findOldestToc = function () {
        var deferred = Q.defer();
        self.db.find({}).sort({ date: 1 }).exec(function (err, newDoc) {
            deferred.resolve(newDoc[0]);
        });
        return deferred.promise;
    }
    
    self.deleteOldest = function (date) {
        var deferred = Q.defer();
        self.db.remove({
            $where: function (date) {
                if (this.date > date && this.date < date + 86400000) // 86400000 - 24 hour
                {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        , {multi: true}, function (err, newDoc) {
            deferred.resolve[newDoc];
        });
        return deferred.promise;
    }
    
    self.getFilteredToc = function (program, grade, nonGradeLevel, nonGradeTitle) {
        var deferred = Q.defer();
        
        if (program === "") { 
            program = null;
        }
        if (grade === "") { 
            grade = null;
        }
        if (nonGradeLevel === "") { 
            nonGradeLevel = null;
        }
        if (nonGradeTitle === "") { 
            nonGradeTitle = null;
        }
	

        self.db.find({
            program: program,
            grade: grade,
            nonGradeLevel: nonGradeLevel,
            nonGradeTitle: nonGradeTitle
            }, { toc: 0 }).sort({ date: -1, environment : -1 }).limit(10).exec(function (err, newDoc) {
            deferred.resolve(newDoc);
        });
        return deferred.promise;
    }
    

    //init
    self.db = new datastore({ filename: 'tocData', autoload: true });
    self.db.loadDatabase();
};


function TocDataObject(toc, environment, program, grade, nonGradeLevel, nonGradeTitile) {
    var self = this;
    self.environment = environment || null;
    self.program = program || null;
    self.grade = grade || null;
    self.nonGradeLevel = nonGradeLevel || null;
    self.nonGradeTitle = nonGradeTitile || null;
    self.date = new Date();
    self.toc = toc;
};

module.exports = TocPesristence;
