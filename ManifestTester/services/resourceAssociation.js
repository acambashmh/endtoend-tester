﻿var http = require('q-io/http');
var querystring = require('querystring');
var utils = require('./utils.js');
var config = require('../Infrastructure/manifestTesterConfig');

var ResourceAssociation = function () {

    
    var processResponse = function (response) {
        var responseData = "";
        utils.errorHandler(response.status);
        return response.body.forEach(function (data) {
            responseData += data;
        }).then(function () {
            return JSON.parse(responseData);
        });
    };
    
    this.getResourceAssociation = function (token, environment, sectionArray, platform) {
        
        var defPath = '/api/caes/v1/resourceAssociation;contextId='+platform+'?sectionRefIds=';
        var options = {
            hostname: config.getHostDataForPlatform().hostUrl,
            path: defPath,
            method: 'GET',
            headers: {
                'Authorization': ''
            }
        };
        var normalizedString = "";
        if (typeof sectionArray == "string") {
            normalizedString = sectionArray.split(',').join('%2C');
        } else {
            normalizedString = sectionArray.join('%2C');
        }
        
        options.path = defPath + normalizedString;
        options.hostname = config.getHostDataForPlatform(environment).hostUrl;
        options.headers.Authorization = token;
        return http.request(options).then(processResponse);
    };
};

module.exports =  ResourceAssociation;