﻿var identity = require('./identity.js');
var http = require('q-io/http');
var utils = require('./utils.js');
var config = require('../Infrastructure/manifestTesterConfig');

var Annotations = function () {
    var normalizeParameters = function (param) {
        if (param)
            return param.split(' ').join('+');
        return "";
    };
    
    var getQueryString = function (params) {
        var qs = "";
        for (var opt in params) {
            if (opt != "program")
                qs += "&";
            qs += opt + "=" + params[opt];
        }
        return qs;
    };
    
    
    var processResponse = function (response) {
        var responseData = "";
        utils.errorHandler(response.status);
        return response.body.forEach(function (data) {
            responseData += data;
        })
        .then(function () {
            return JSON.parse(responseData);
        });
    };
    
    
    var defPath = '/api/data/services/annotations?';
    
    var options = {
        hostname: config.getHostDataForPlatform().hostUrl,
        path: defPath,
        headers: {
            'Authorization': ''
        }
    };
    
    
    this.getAnnotations = function (token, environment, $select, $filter) {


        var params = {
            $select : normalizeParameters($select),
            $filter: normalizeParameters($filter),
        };
        options.hostname = config.getHostDataForPlatform(environment).hostUrl;
        options.path = defPath + getQueryString(params);
        options.headers.Authorization = token;
        options.headers.Accept = 'application/json';
        return http.request(options).then(processResponse);
    }
    
}

module.exports =  Annotations;