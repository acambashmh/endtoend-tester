﻿// Basic module for getting access_token from identity service
var http = require('q-io/http');
var querystring = require('querystring');
var config = require('../Infrastructure/manifestTesterConfig')
var q = require('q');

var Identity = function () {
    
    var configData = config.getConfigDataSync();
    
    var processResponse = function (response) {
        var responseData = "";
        if (response.status == 400) {
            throw new Error("username/password combination is incorrect")
        }
        return response.body.forEach(function (data) {
            responseData += data;
        }).then(function () {
                return JSON.parse(responseData);
            });
    };

    this.getIdentityData = function (username, password, environment, platform) {
        
        var host = config.getHostDataForPlatform(environment);
        var options = createOptions(username, password, host.hostUrl, platform);
        return http.request(options).then(function (response) {
            var responseData = "";
            if (response.status == 400) {
                throw new Error("username/password combination is incorrect")
            }
            return response.body.forEach(function (data) {
                responseData += data;
            }).then(function () {
                    var identity = JSON.parse(responseData);
                    identity.environment = host.name;
                    return identity;
                });
        });

    };
    
    
    function createOptions(username, password, hostUrl, platform) {
        
        var user = {
            username: username || configData.hostData.username ,
            password: password || configData.hostData.password,
            grant_type: 'password'
        };
        
        if (platform == 'tc') {
            user.username = 'uid=' + user.username + ',o=88200010,dc=88200009,st=CA';
        }
        
        var options = {
            hostname: hostUrl || configData.hostData.hostUrl,
            path: '/api/identity/v1/token;contextId=' + platform,
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Content-Length': Buffer.byteLength(querystring.stringify(user))
            },
            body: [querystring.stringify(user)]
        }
        
        return options;
    }

}

module.exports = Identity;