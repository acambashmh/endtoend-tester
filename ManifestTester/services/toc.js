﻿var identity = require('./identity.js');
var http = require('q-io/http');
var utils = require('./utils.js');
var config = require('../Infrastructure/manifestTesterConfig');

var TOC = function () {
    var normalizeParameters = function (param) {
        if(param)
            return param.split(' ').join('+');
        return "";
    };
    
    var getQueryString = function (params) {
        var qs = "";
        for (var opt in params) {
            if (opt != "program")
                qs += "&";
            qs += opt + "=" + params[opt];
        }
        return qs;
    };
    

    var processResponse = function (response) {
        var responseData = "";
        utils.errorHandler(response.status);
        return response.body.forEach(function (data) {
            responseData += data;
        })
        .then(function () {
            return JSON.parse(responseData);
        });
    };
    
    var defPath = '/api/ugen/toc?';

    var options = {
        hostname: config.getHostDataForPlatform().hostUrl,
        path: defPath,
        headers: {
            'Authorization': ''
        }
    };

    
    this.getTOC = function (token, environment, program, grade, nonGradeLevel, nonGradeTitile, version, showResource, json) {
        var params = {
            program : normalizeParameters(program),
            grade: normalizeParameters(grade),
            non_grade_level: normalizeParameters(nonGradeLevel),
            non_grade_title: normalizeParameters(nonGradeTitile),
            version: normalizeParameters(version),
            showResources: normalizeParameters(showResource),
            outputFormat: normalizeParameters(json)
        };
        options.hostname = config.getHostDataForPlatform(environment).hostUrl;
        options.path = defPath + getQueryString(params);
        options.headers.Authorization = token;

        return http.request(options).then(processResponse);
    }
    

    this.getTocData = function () { 
        
    }
}

module.exports =  TOC;