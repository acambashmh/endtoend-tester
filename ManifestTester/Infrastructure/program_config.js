var tocConfig ={
    "tocData": {
        "int": [
            {
                "program": "CA Go Math!",
                "grade": "1",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "pcteacherg5",
                "password": "Password@1",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "2",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "pcteacherg5",
                "password": "Password@1",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "3",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "pcteacherg5",
                "password": "Password@1",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "4",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "pcteacherg5",
                "password": "Password@1",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "5",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "pcteacherg5",
                "password": "Password@1",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "6",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "pcteacherg5",
                "password": "Password@1",
                "platform": "tc"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "6",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "g6student1",
                "password": "password",
                "platform": "hmof"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "7",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "rvanpersie",
                "password": "password",
                "platform": "hmof"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "8",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "tmurman",
                "password": "password",
                "platform": "hmof"
            },
            {
                "program": "Accelerated Grade 7",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Accelerated Grade 7",
                "username": "rmanning142",
                "password": "password",
                "platform": "hmof"
            },
            {
                "program": "Accelerated Grade 7",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "CA Algebra 1: Analyze, Connect, Explore",
                "username": "jtheman",
                "password": "password",
                "platform": "hmof"
            },
            {
                "program": "CA High School Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Algebra 1",
                "username": "ent8173sixyr",
                "password": "ent8173sixyr",
                "platform": "hmof"
            },
            {
                "program": "CA High School Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Algebra 2",
                "username": "ent8173sixyr",
                "password": "ent8173sixyr",
                "platform": "hmof"
            },
            {
                "program": "CA High School Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Geometry",
                "username": "ent8173sixyr",
                "password": "ent8173sixyr",
                "platform": "hmof"
            },
            {
                "program": "CA High School Integrated Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Integrated Mathematics 1",
                "username": "ent8173sixyr",
                "password": "ent8173sixyr",
                "platform": "hmof"
            },
            {
                "program": "CA High School Integrated Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Integrated Mathematics 2",
                "username": "ent8173sixyr",
                "password": "ent8173sixyr",
                "platform": "hmof"
            }
        ],
        "certReview": [
            {
                "program": "CA Go Math!",
                "grade": "1",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "crplyrteacher1",
                "password": "password",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "2",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "crplyrteacher1",
                "password": "password",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "3",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "crplyrteacher1",
                "password": "password",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "4",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "crplyrteacher1",
                "password": "password",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "5",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "crplyrteacher1",
                "password": "password",
                "platform": "tc"
            },
            {
                "program": "CA Go Math!",
                "grade": "6",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "crplyrteacher1",
                "password": "password",
                "platform": "tc"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "6",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "Gobrien1234",
                "password": "Gobrien1234",
                "platform": "hmof"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "7",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "Gobrien1234",
                "password": "Gobrien1234",
                "platform": "hmof"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "8",
                "non_grade_level": "",
                "non_grade_title": "",
                "username": "Gobrien1234",
                "password": "Gobrien1234",
                "platform": "hmof"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Accelerated Grade 7",
                "username": "Gobrien1234",
                "password": "Gobrien1234",
                "platform": "hmof"
            },
            {
                "program": "CA Go Math! 2015",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "CA Algebra 1: Analyze, Connect, Explore",
                "username": "Gobrien1234",
                "password": "Gobrien1234",
                "platform": "hmof"
            },
            {
                "program": "CA High School Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Algebra 1",
                "username": "Ent7701",
                "password": "Ent77011",
                "platform": "hmof"
            },
            {
                "program": "CA High School Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Geometry",
                "username": "Ent7701",
                "password": "Ent77011",
                "platform": "hmof"
            },
            {
                "program": "CA High School Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Algebra 2",
                "username": "Ent7701",
                "password": "Ent77011",
                "platform": "hmof"
            },
            {
                "program": "CA High School Integrated Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Integrated Mathematics 1",
                "username": "Ent7701",
                "password": "Ent77011",
                "platform": "hmof"
            },
            {
                "program": "CA High School Integrated Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Integrated Mathematics 2",
                "username": "Ent7701",
                "password": "Ent77011",
                "platform": "hmof"
            },
            {
                "program": "CA High School Integrated Math",
                "grade": "",
                "non_grade_level": "",
                "non_grade_title": "Integrated Mathematics 3",
                "username": "Ent7701",
                "password": "Ent77011",
                "platform": "hmof"
            }
        ]
    }

}


module.exports = tocConfig;