﻿var xls = require('xlsx');
var _ = require('underscore');
var TocGenerator = function (path) {
    this.path = path;
    this.LLENGTH = 7;
    this._createWhereQuery = function (first, array, value) {
        var obj = {};
        obj[first] = value;
        for (var i = 0; i < array.length; i++) {
            obj[array[i]] = '';
        }
        return obj;
    };
    
    this._getNonEmptyColumns = function (array) {
        var columns = new Array();
        for (var i = 1; i < array.length; i++) {
            if (array[i] > 0) {
                columns.push('L' + i);
            }
        }
        return columns;
    };
    
    this._convertXlsArrayToResourceArray = function (xlsArray) {
        var resourceArray = new Array();
        for (var i = 0, len = xlsArray.length; i < len; i++) {
            var resourceObject = new Object();
            resourceObject.id = xlsArray[i].id;
            resourceObject.HMH_ID = xlsArray[i].HMH_ID;
            resourceObject.display_title = xlsArray[i].display_title;
            resourceObject.component_type = xlsArray[i].component_type;
            resourceObject.media_type = xlsArray[i].media_type;
            resourceObject.categorization = xlsArray[i].categorization;
            resourceObject.download_url = xlsArray[i].download_url;
            resourceObject.tool_type = xlsArray[i].tool_type;
            resourceObject.assignable = xlsArray[i].assignable;
            resourceObject.ISBN = new Array();
            resourceObject.ISBN.push(xlsArray[i].ISBN);
            resourceObject.uri = xlsArray[i].uri;
            resourceArray.push(resourceObject);
        }
        return resourceArray;
    }
    
    this._sortXlsRows = function (a, b) {
        for (var i = 0, len = this.LLENGTH; i < len; i++) {
            if (a['L' + i] != b['L' + i]) {
                return a['L' + i] - b['L' + i];
            }
        }
        return a['L' + (this.LLENGTH - 1)] - b['L' + (this.LLENGTH - 1)];
    }
    
    _.groupByMulti = function (obj, values, context) {
        if (!values.length)
            return obj;
        var typeID = +values[0][1];
        var les = new Object();
        les.level = new Array();
        var object = (obj.byFirst)?obj.byFirst:obj;
        les.byFirst = _.groupBy(object, values[0], context);
        var rest = values.slice(1);
        for (var prop in les.byFirst) {
            var title = les.byFirst[prop][0].LTitles[typeID];
            var type = context.types[typeID];
            les.byFirst[prop] = _.groupByMulti(les.byFirst[prop], rest, context);
            if (prop != "") {
                var levelObj = new Object;
                levelObj.number = values[0];
                levelObj.hierarchy = prop;
                levelObj.title = title;
                levelObj.type = type.toLowerCase();
                levelObj.resources = new Object();
                levelObj.resources.resource = context._convertXlsArrayToResourceArray(_.where(les.byFirst[prop], context._createWhereQuery(values[0], rest, +prop)));
                if (les.byFirst[prop].level) {
                    levelObj.level = les.byFirst[prop].level;
                }
                
                les.level.push(levelObj);
            }
        }
        return les;
    }
    
    _.groupByMultiAdditional = function (obj, values, group, context) {
        if (!group.length)
            return obj;
        var typeID = +values[0][1];
        var les = new Object();
        les.level = new Array();
        var object = (obj.byFirst)?obj.byFirst:obj;
        les.byFirst = _.groupBy(object, group[0], context);
        var rest = values.slice(1);
        var restG = group.slice(1);
        for (var prop in les.byFirst) {
            var title = les.byFirst[prop][0].LTitles[typeID].split(':')[typeID % 2];
            var type = context.types[typeID];
            les.byFirst[prop] = _.groupByMultiAdditional(les.byFirst[prop], rest, restG, context);
            if (prop != "") {
                var levelObj = new Object;
                levelObj.title = prop;
                levelObj.type = type.toLowerCase();
                levelObj.resources = new Object();
                levelObj.resources.resource = context._convertXlsArrayToResourceArray(_.where(les.byFirst[prop], context._createWhereQuery(group[0], restG, prop)));
                if (les.byFirst[prop].level) {
                    levelObj.level = les.byFirst[prop].level;
                }
                
                les.level.push(levelObj);
            }
        }
        return les;
    }
    
    this.generate = function () {
        var workbook = xls.readFile(this.path);
        var sheetName = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[sheetName];
        var startRow = 5;
        var numOfRows = +worksheet['!ref'].split(':')[1].replace(new RegExp('[A-Z]+'), '');
        var isbn = worksheet['A' + 2].v;
        var platformUrl = worksheet['H' + 2].v;
        var grade = worksheet['D' + 2].v;
        var nonGradeTitle = (worksheet['Z' + 5]) ? worksheet['Z' + 5].v : "";
        var nonGradeLevel = (worksheet['CS' + 5]) ? worksheet['CS' + 5].v : "";
        this.types = [worksheet['S1'].v, worksheet['T1'].v, worksheet['U1'].v, worksheet['V1'].v, worksheet['W1'].v, worksheet['X1'].v, worksheet['Y1'].v];
        var isEmpty = [0, 0, 0, 0, 0, 0, 0];
        this.xlsRows = new Array();
        var additional = new Array();
        for (var i = startRow; i < numOfRows; i++) {
            var xlsRow = new Object();
            xlsRow.id = (worksheet['AY' + i]) ? worksheet['AY' + i].v : "";
            xlsRow.HMH_ID = (worksheet['AM' + i]) ? worksheet['AM' + i].v : "";
            xlsRow.ISBN = new Object();
            xlsRow.ISBN.type = 'primary';
            xlsRow.ISBN.value = isbn;
            xlsRow.assignable = (worksheet['AQ' + i]) ? true : false;
            xlsRow.categorization = (worksheet['BD' + i]) ? worksheet['BD' + i].v : "";
            xlsRow.component_type = (worksheet['BC' + i]) ? worksheet['BC' + i].v : "";
            xlsRow.display_title = (worksheet['AL' + i]) ? worksheet['AL' + i].v : "";
            xlsRow.download_url = (worksheet['CU' + i]) ? worksheet['CU' + i].v : "";
            xlsRow.id = (worksheet['AM' + i]) ? worksheet['AM' + i].v : "";
            xlsRow.media_type = (worksheet['BE' + i]) ? worksheet['BE' + i].v : "";
            xlsRow.tool_type = (worksheet['CH' + i]) ? worksheet['CH' + i].v : "";
            xlsRow.uri = new Object();
            xlsRow.uri.platform_url = platformUrl;
            xlsRow.uri.relative_url = (worksheet['AV' + i]) ? worksheet['AV' + i].v : "";
            xlsRow.instructionalSegment = (worksheet['B' + i]) ? worksheet['B' + i].v : "";
            lessonPlanTitle = (worksheet['G' + i]) ? worksheet['G' + i].v.split(':') : "";
            xlsRow.lessonPlanTitleTitle = (lessonPlanTitle[0]) ? lessonPlanTitle[0] : '';
            xlsRow.lessonPlanTitleLesson = (lessonPlanTitle[1]) ? lessonPlanTitle[1] : '';
            
            
            xlsRow.L0 = ((worksheet['S' + i]) ? +worksheet['S' + i].v : "");
            xlsRow.L1 = (worksheet['T' + i]) ? +worksheet['T' + i].v : "";
            xlsRow.L2 = (worksheet['U' + i]) ? +worksheet['U' + i].v : "";
            xlsRow.L3 = (worksheet['V' + i]) ? +worksheet['V' + i].v : "";
            xlsRow.L4 = (worksheet['W' + i]) ? +worksheet['W' + i].v : "";
            xlsRow.L5 = (worksheet['X' + i]) ? +worksheet['X' + i].v : "";
            xlsRow.L6 = (worksheet['Y' + i]) ? +worksheet['Y' + i].v : "";
            
            for (var cnt = 0; cnt < isEmpty.length; cnt++) {
                if (xlsRow['L' + cnt] != '') {
                    isEmpty[cnt]++;
                }
            }
            
            xlsRow.LTitles = new Array();
            xlsRow.LTitles.push((worksheet['Z' + i]) ? worksheet['Z' + i].v : "");
            xlsRow.LTitles.push((worksheet['AA' + i]) ? worksheet['AA' + i].v : "");
            xlsRow.LTitles.push((worksheet['AB' + i]) ? worksheet['AB' + i].v : "");
            xlsRow.LTitles.push((worksheet['AC' + i]) ? worksheet['AC' + i].v : "");
            xlsRow.LTitles.push((worksheet['AD' + i]) ? worksheet['AD' + i].v : "");
            xlsRow.LTitles.push((worksheet['AE' + i]) ? worksheet['AE' + i].v : "");
            xlsRow.LTitles.push((worksheet['AF' + i]) ? worksheet['AF' + i].v : "");
            if (xlsRow.instructionalSegment.toLowerCase().indexOf('additional') > -1) {
                additional.push(xlsRow);
            }
            else {
                this.xlsRows.push(xlsRow);
            }
        }
        
        this.xlsRows.sort(this._sortXlsRows);
        
        var toc = new Object();
        toc.levels = {};
        toc.levels.level = [];
        toc.levels.level[0] = new Object();
        toc.levels.level[0].level_number = '0';
        toc.levels.level[0].nonGradeTitle = nonGradeTitle;
        toc.levels.level[0].nonGradeLevel = nonGradeLevel;
        toc.levels.level[0].number = 'L0';
        toc.levels.level[0].level = new Array();
        toc.levels.level[0].resources = new Object();
        var levels = new Array();
        
        var nonEmptyColumns = this._getNonEmptyColumns(isEmpty);
        if (nonEmptyColumns.length == 2) {
            var level = new Object();
            level.level = _.groupByMulti(this.xlsRows, nonEmptyColumns, this).level;
            level.level = _.union(level.level, _.groupByMultiAdditional(additional, nonEmptyColumns, ['lessonPlanTitleTitle', 'lessonPlanTitleLesson'], this).level)
            toc.levels.level[0].level.push(level);
        }
        else {
            toc.levels.level[0].resources.resource = this._convertXlsArrayToResourceArray(_.where(this.xlsRows, this._createWhereQuery('L0', nonEmptyColumns, grade)));
            toc.levels.level[0].level = _.groupByMulti(this.xlsRows, nonEmptyColumns, this).level;
        }
        return toc;
    }
}


module.exports = TocGenerator;