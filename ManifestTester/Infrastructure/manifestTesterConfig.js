﻿var Q = require('q');
var fs = require('fs');
var file = './config.json';

function getConfigData() {
    return Q.nfcall(fs.readFile, file, "utf-8")
    .then(function (data) {
        return JSON.parse(data)
    })
    .catch(function (err) {
        console.log(err)
    });
}


function getConfigDataSync(fileUrl) {
    var res;
    
    var fileUrl = fileUrl || file;
    res = fs.readFileSync(fileUrl,'utf-8')
    res = JSON.parse(res);
    
    return res;
}

function getHostDataForPlatform(environment) {
    if (!environment) { 
        return getConfigDataSync().hostData;
    }
    var result;
    if (environment == 'int') {
        result = getConfigDataSync().hostList[0];
    } else if (environment == 'cert-review') {
        result =  getConfigDataSync().hostList[1];
    } else if (environment == 'cert') {
        result = getConfigDataSync().hostList[2];
    } else { 
        result = getConfigDataSync().hostList[0];
    }
    return result;

}



exports.getConfigData = getConfigData;

exports.getConfigDataSync = getConfigDataSync;

exports.getHostDataForPlatform = getHostDataForPlatform;