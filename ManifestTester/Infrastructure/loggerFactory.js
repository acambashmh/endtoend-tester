﻿function Logger() {
    var self = this;

    self.info = function (data) { 
    }

    self.warn = function (data) { 
    }

    self.error = function (data) { 
    }

}


function ConsoleLogger() {
    ConsoleLogger.prototype = new Logger();
    ConsoleLogger.prototype.constructor = ConsoleLogger;

    var self = this;

    self.info = function (data) {
        console.info(data);
    }
    
    self.warn = function (data) {
        console.warn(data);
    }
    
    self.error = function (data) {
        console.error(data);
    }
}



function createConsoleLogger() { 
    return new ConsoleLogger();
}


exports.createConsoleLogger = createConsoleLogger;

