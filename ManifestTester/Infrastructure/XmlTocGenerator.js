﻿var fs = require('fs'),
    xml2js = require('xml2js'),
    TocGenerator = require('./TOCGenerator.js'),
    _ = require('underscore');
var MDSXlsToTOC = function (path) {
    this.path = path;
    var types = ["", "", "", "", "", "", ""];
    var createLs = function (level, xlsRow) {
        if (level.level) {
            xlsRow = createLs(level.level[0], xlsRow);
        }
        else {
            xlsRow.instructionalSegment = level.instructional_segment[0].title[0];
        }
        xlsRow['L' + level.$.level_number] = +level.$.hierarchy;
        xlsRow.LTitles[+level.$.level_number] = level.title[0];
        types[+level.$.level_number] = level.$.type;
        return xlsRow;
    }
    var parser = new xml2js.Parser();
    this.generate = function (res) {
        fs.readFile(this.path, function (err, data) {
            parser.parseString(data, function (err, result) {
                console.dir(result);
                var resources = result.MDS.resources[0].resource;
                var lessonPlans = result.MDS.lesson_plans[0].lesson_plan;
                var additional = new Array();
                var xlsRows = new Array();
                isEmpty = [0, 0, 0, 0, 0, 0, 0];
                for (var i = 0, len = resources.length; i < len; i++) {
                    var xlsRow = new Object();
                    xlsRow.id = resources[i].$.id;
                    xlsRow.HMH_ID = resources[i].$.HMH_ID;
                    xlsRow.ISBN = new Object();
                    xlsRow.ISBN.type = resources[i].setOfISBNs[0].ISBN[0].$.type;
                    xlsRow.ISBN.value = resources[i].setOfISBNs[0].ISBN[0]._;
                    xlsRow.assignable = resources[i].assignable[0];
                    xlsRow.categorization = resources[i].categorization[0];
                    xlsRow.component_type = resources[i].component_type[0];
                    xlsRow.display_title = resources[i].display_title[0];
                    xlsRow.download_url = resources[i].download_url[0];
                    xlsRow.id = resources[i].$.HMH_ID;
                    xlsRow.media_type = resources[i].media_type[0];
                    xlsRow.tool_type = resources[i].tool_type[0];
                    xlsRow.uri = resources[i].uri[0];
                    if (lessonPlans[i]) {
                        lessonPlanTitle = lessonPlans[i].title;
                    }
                    xlsRow.lessonPlanTitleTitle = (lessonPlanTitle[0]) ? lessonPlanTitle[0] : '';
                    xlsRow.lessonPlanTitleLesson = (lessonPlanTitle[1]) ? lessonPlanTitle[1] : '';
                    xlsRow.L0 = "";
                    xlsRow.L1 = "";
                    xlsRow.L2 = "";
                    xlsRow.L3 = "";
                    xlsRow.L4 = "";
                    xlsRow.L5 = "";
                    xlsRow.L6 = "";
                    
                    xlsRow.LTitles = ["", "", "", "", "", "", ""];
                    createLs(resources[i].level[0], xlsRow);
                    
                    for (var cnt = 0; cnt < isEmpty.length; cnt++) {
                        if (xlsRow['L' + cnt] != '') {
                            isEmpty[cnt]++;
                        }
                    }
                    if (xlsRow.instructionalSegment.toLowerCase().indexOf('additional') > -1) {
                        additional.push(xlsRow);
                    } else {
                        xlsRows.push(xlsRow);
                    }
                }
                console.log('Done', xlsRows);
                
                var toc = new Object();
                toc.levels = {};
                toc.levels.level = [];
                toc.levels.level[0] = new Object();
                toc.levels.level[0].level_number = '0';
                toc.levels.level[0].nonGradeTitle = resources[0].non_grade_title[0];
                toc.levels.level[0].nonGradeLevel = resources[0].non_grade_level[0];
                toc.levels.level[0].number = 'L0';
                toc.levels.level[0].level = new Array();
                toc.levels.level[0].resources = new Object();
                var levels = new Array();
                var tocGenerator = new TocGenerator();
                nonEmptyColumns = tocGenerator._getNonEmptyColumns(isEmpty);
                tocGenerator.types = types;
                if (nonEmptyColumns.length == 2) {
                    var level = new Object();
                    level.level = _.groupByMulti(xlsRows, nonEmptyColumns, tocGenerator).level;
                    level.level = _.union(level.level, _.groupByMultiAdditional(additional, nonEmptyColumns, ['lessonPlanTitleTitle', 'lessonPlanTitleLesson'], tocGenerator).level)
                    toc.levels.level[0].level.push(level);
                }
                else {
                    toc.levels.level[0].resources.resource = tocGenerator._convertXlsArrayToResourceArray(_.where(xlsRows, tocGenerator._createWhereQuery('L0', nonEmptyColumns, xlsRows[0].L0)));
                    toc.levels.level[0].level = _.groupByMulti(xlsRows, nonEmptyColumns, tocGenerator).level;
                }
                res.send(toc);
            });
        });
    }
}

module.exports = MDSXlsToTOC;