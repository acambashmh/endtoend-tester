﻿var q = require('q');
var _ = require('underscore');
var request = require('request');
var config = require('./program_config.js');
var tocService = require('../services/toc');
var identity = require('../services/identity');
var tocPersistance = require('../services/tocPersistence');
var CronJob = require('cron').CronJob;

var getAllToc = function () {
    var promiseInt = getAllTocFromHostList('int', config.int);
    var promiseCertReview = getAllTocFromHostList('cert-review', config.certReview);
    return q.all([promiseInt, promiseCertReview]).then(function (data) {
        var allTocList = []
        _.each(data, function (enviromentTocList) {
            _.each(enviromentTocList, function (tocData) {
                allTocList.push(tocData);
            });
        })
        console.log('all toc received');
        return allTocList;
    });
}

var getAllTocFromHostList = function (environment, tocParamsList) {
    var tocPromiseList = [];
    var identityService = new identity();
    _.each(tocParamsList, function (tocParamsItem) {
        (function () {
            var defer = q.defer();
            var promise = defer.promise;
            identityService.getIdentityData(tocParamsItem.username, tocParamsItem.password, environment, tocParamsItem.platform)
            .then(function (token) {
                    var toc = new tocService().getTOC(token.access_token, environment, tocParamsItem.program, tocParamsItem.grade, 
                        tocParamsItem.non_grade_level, tocParamsItem.non_grade_title, 'player', 'true', 'json')
                    .then(function (data) {
                            var tocData = {};
                            tocData.environment = environment || null;
                            tocData.program = tocParamsItem.program || null;
                            tocData.grade = tocParamsItem.grade || null;
                            tocData.nonGradeLevel = tocParamsItem.non_grade_level || null;
                            tocData.nonGradeTitle = tocParamsItem.non_grade_title || null;
                            tocData.toc = data;
                            tocData.date = new Date();
                            
                            defer.resolve(tocData);
                        }, function (err) {
                            var tocData = {};
                            tocData.environment = environment || null;
                            tocData.program = tocParamsItem.program || null;
                            tocData.grade = tocParamsItem.grade || null;
                            tocData.nonGradeLevel = tocParamsItem.non_grade_level || null;
                            tocData.nonGradeTitle = tocParamsItem.non_grade_title || null;
                            tocData.error = 'error getting the toc';
                            tocData.date = new Date();
                            tocData.toc = {};
                            defer.resolve(tocData);

                        });
                }, function (err) {
                    var tocData = {};
                    tocData.environment = environment || null;
                    tocData.program = tocParamsItem.program || null;
                    tocData.grade = tocParamsItem.grade || null;
                    tocData.nonGradeLevel = tocParamsItem.non_grade_level || null;
                    tocData.nonGradeTitle = tocParamsItem.non_grade_title || null;
                    tocData.date = new Date();
                    tocData.error = 'login error';
                    tocData.toc = {};
                    defer.resolve(tocData);
                })
            
            tocPromiseList.push(promise);
        }());
    });
    return q.all(tocPromiseList);
}

var saveTocList = function (tocList) {
    var promiseList = [];
    var tocListLength = tocList.length;
    var persistanece = new tocPersistance();
    console.log('start persist toc list');
    _.each(tocList, function (tocData) {
        (function () {
            var defer = q.defer();
            var promise = defer.promise;
            persistanece.insertToc(tocData.toc, tocData.environment, tocData.program, tocData.grade, tocData.nonGradeLevel, tocData.nonGradeTitle)
            .then(function () {
                    defer.resolve();
                });
            
            promiseList.push(promise);
        }());
    });
    
    return q.all(promiseList);
}

var getAndSaveAll = function () {
    return getAllToc()
    .then(function (data) {
            console.log('save toc start');
            var date = new Date();
            console.log('date : ' + date.toDateString() + ' time: ' + date.toTimeString());
            return saveTocList(data);
        })
    .then(function () {
            console.log('end persist toc list');
            console.log('save toc end')
            var date = new Date();
            console.log('date : ' + date.toDateString() + ' time: ' + date.toTimeString());
        })
    .catch(function (e) {
            console.log(e);
        });
}

var startGetAllTask = function () {
    var job = new CronJob('00 53 15 * * 1-5', function () {
        console.log('save toc job start');
        var date = new Date();
        console.log('date : ' + date.toDateString() + ' time: ' + date.toTimeString());
        getAndSaveAll();
    }, function () {

    },
      true 
    );
}


exports.getAllToc = getAllToc;
exports.saveTocList = saveTocList;
exports.startGetAllTask = startGetAllTask;
exports.getAndSaveAll = getAndSaveAll;
