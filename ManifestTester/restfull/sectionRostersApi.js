﻿var sectionRosters = require('./../services/sectionRosters.js');
var config = require('../Infrastructure/manifestTesterConfig');
var _ = require('underscore');


exports.addSectionRosetersApiToServer = addApiToServer;

function addApiToServer(server) {
    server.get('/sectionrosters', getSectionRosters);
    server.opts('/sectionrosters', optionHandle);
}


function optionHandle(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Headers', "token, platform, i, environment");
    res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE");
    res.setHeader('Access-Control-Max-Age', "1728000");
    res.send(200);
    next();
}

var getSectionRosters = function (req, res, next) {
    var sr = new sectionRosters();
    sr.getSectionRosters(req.headers.token, req.headers.environment,req.params.refId, req.headers.platform ).then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
};
