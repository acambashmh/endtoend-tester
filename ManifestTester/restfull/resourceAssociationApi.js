﻿var resourceAssociation = require('./../services/resourceAssociation.js');
var config = require('../Infrastructure/manifestTesterConfig');
var _ = require('underscore');

exports.addResourceAssociationApiToServer = addTocApiToServer;

function addTocApiToServer(server) {
    server.get('/resourceassociation', getResourceAssociation);
    server.opts('/resourceassociation', optionHandle);
}

function optionHandle(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Headers', "token, platform, i, environment");
    res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE");
    res.setHeader('Access-Control-Max-Age', "1728000");
    res.send(200);
    next();
}

var getResourceAssociation = function (req, res, next) {
    var r = new  resourceAssociation();
    r.getResourceAssociation(req.headers.token, req.headers.environment,  req.params.sectionRefIds, req.headers.platform).then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
};
