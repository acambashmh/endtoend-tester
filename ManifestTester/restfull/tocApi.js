﻿var fs = require('fs');
var toc = require('./../services/toc.js');
var tocDP = require('./../services/tocPersistence.js');
var resourceCheck = require('./../services/resourceCheck.js');
var tocValidatorFactory = require('../Validation/tocValidator');
var config = require('../Infrastructure/manifestTesterConfig');
var tocDP = new tocDP();
exports.addTocApiToServer = addTocApiToServer;

function addTocApiToServer(server) {
    server.get('/validate/toc', validateToc);
    server.opts('/validate/toc', optionHandle);
    server.get('/toc', getToc);
    server.opts('/toc', optionHandle);
    server.post('/savetoc', saveToc);
    server.opts('/savetoc', optionHandle);
    server.get('/toclist', getTocList);
    server.opts('/toclist', optionHandle);
    server.get('/tocbyid', getTocById);
    server.opts('/tocbyid', optionHandle);
    //getTocFilteredList
    server.get('/tocfilteredlist', getTocFilteredList);
    server.opts('/tocfilteredlist', optionHandle);

    server.post('/toccheckresources', checkTocResources);
    server.opts('/toccheckresources', optionHandle);

    server.get('/tocSettings', tocSettings);
}


//getTocById

function optionHandle(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Headers', "token, platform, i, environment");
    res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE");
    res.setHeader('Access-Control-Max-Age', "1728000");
    res.send(200);
    next();
}

function validateToc(req, res, next) {
    var tocValidator = tocValidatorFactory.getTocValidator();
    
    var t = new toc();

    t.getTOC(req.headers.token, req.headers.environment, req.params.program, req.params.grade, 
        req.params.non_grade_level, req.params.non_grade_title, req.params.version, req.params.showResources, req.params.outputFormat)
    .then(function (data) {
        return tocValidator.validate(data);
    })
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}

function getToc(req, res, next) {
    var t = new toc();
    t.getTOC(req.headers.token, req.headers.environment, req.params.program, req.params.grade, 
        req.params.non_grade_level, req.params.non_grade_title, 
        req.params.version, req.params.showResources, req.params.outputFormat)
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}


function saveToc(req, res, next) {
    var tDp = tocDP;
    tDp.insertToc(req.body.toc, req.body.environment, req.body.program, req.body.grade, 
        req.body.non_grade_level, req.body.non_grade_title)
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}


function checkTocResources(req, res, next) {
    resourceCheck.checkAllTocResources(req.body.toc, req.body.enviroments, req.body.platform)
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}

function getTocList(req, res, next) {
    var t = new toc();
    var tDp = tocDP;
    tDp.getTocList()
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}


function getTocFilteredList(req, res, next) {
    var t = new toc();
    var tDp = tocDP;
    tDp.getFilteredToc(req.params.program, req.params.grade, req.params.nonGradeLevel, req.params.nonGradeTitle)
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}

function getTocById(req, res, next) {
    var t = new toc();
    var tDp = tocDP;
    tDp.getTocById(req.params.id)
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}

function tocSettings(req, res, next) {
    var result = config.getConfigDataSync().tocDefaultValues;
    res.header('Access-Control-Allow-Origin', "*");
    res.send(result);
    next();
}

function handleError(data) { 
    console.log(data);
}