﻿var identity = require('./../services/identity.js');
var restify = require('restify');
var assignmentApi = require('./assignmentApi.js');
var tocApi = require('./tocApi.js');
var config = require('../Infrastructure/manifestTesterConfig');
var sectionsApi = require('./sectionsApi.js');
var annotationsApi = require('./annotationsApi.js');
var checkAllApi = require('./checkAllApi.js');
var sectionRosetersApi = require('./sectionRostersApi.js');
var resourceAssociationApi = require('./resourceAssociationApi.js');
var tocGen = require('../Infrastructure/TOCGenerator.js');
var xlsToToc = require('../Infrastructure/XmlTocGenerator.js');
var resourceCheck = require('../services/resourceCheck');
var fs = require('fs');
var standardsValidation = require('../Validation/standardsValidation');


//var fs = require('fs-extra');
//Set up server
var server = restify.createServer();
server.use(restify.dateParser());
server.use(restify.bodyParser());
server.use(restify.queryParser());

var login = function (req, res, next) {
    new identity().getIdentityData(req.body.username, req.body.password, req.body.environment, req.body.platform).then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
};

var getDefaultEnivromentData = function (req, res, next) {
    var result = config.getConfigDataSync().defaultUserList;
    res.header('Access-Control-Allow-Origin', "*");
    res.send(result);
    next();
};



//Handle routes
server.use(function (req, res, next) {
    req.connection.setTimeout(700 * 1000);
    res.connection.setTimeout(700 * 1000);
    next();
});
server.get('/getDefaultEnivromentData', getDefaultEnivromentData);
server.post('/login', login);

server.post('/upload', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    var toc = new tocGen(req.files.file.path).generate();
    res.send(toc);
});

server.post('/uploadXML', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    var toc = new xlsToToc(req.files.file.path).generate(res);
   // res.send(toc);
});


server.post('/checkTocManifestStandards', function (req, res, next) {
    //res.header('Access-Control-Allow-Origin', "*");
    var rawStandards = fs.readFileSync(req.files.file.path, 'utf-8');
    var toc = JSON.parse(req.body.toc);
    resourceCheck.checkStandardsInToc(toc, req.body.environment, req.body.platform, rawStandards)
    .then(function (data) {
        res.send(data);
        next();
    }).fail(function (data) {
        res.send(data);
        next();
    });
});

tocApi.addTocApiToServer(server);
assignmentApi.addAssignmentApiToServer(server);
sectionsApi.addSectionsApiToServer(server);
resourceAssociationApi.addResourceAssociationApiToServer(server);
sectionRosetersApi.addSectionRosetersApiToServer(server);
annotationsApi.addAnnotationsApiToServer(server);
checkAllApi.addCheckAllApiToServer(server);





module.exports.start = function (production) {
    if (production) {
        server.get(/.*/, restify.serveStatic({
            'directory': './webApp/app/dist',
            'default': 'index.html'
        }));
    } else { 
        server.get(/.*/, restify.serveStatic({
            'directory': './webApp/app',
            'default': 'index.html'
        }));
    }

    //START SERVER
    server.listen(8080, function () {
        console.log('%s listening at %s', server.name, server.url);
    });

}