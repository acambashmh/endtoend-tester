﻿var annotations = require('./../services/annotations.js');
var config = require('../Infrastructure/manifestTesterConfig');
exports.addAnnotationsApiToServer = addApiToServer;

function addApiToServer(server) {
    server.get('/annotations', getAnnotations);
}


function getAnnotations(req, res, next) {
    var a = new annotations();
    a.getAnnotations(req.headers.token, req.headers.environment, 
        req.params.$select, req.params.$filter)
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        handleError(data);
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}




function handleError(data) {
    console.log(data);
}