﻿var config = require('../Infrastructure/manifestTesterConfig');

exports.addCheckAllApiToServer = addCheckAllApiToServer;


function addCheckAllApiToServer(server) {
    server.get('/getAllServices', getAllServices);
    server.opts('/getAllServices', optionHandle);
}

var getAllServices = function (req, res, next) {
    var result = [];
    var tocDataHMOF = config.getConfigDataSync().tocDefaultValues;
    var tocDataTc = config.getConfigDataSync().tocDefaultValuesTc;
    var assignmentData = { sectionId: 'f7885719-3015-4673-8ba5-e544e29073c5' };
    
    
    if (req.params.environment === 'cert-review' && req.params.platform === 'hmof') {
        result.push(new Endpoint('toc', '/toc', tocDataHMOF));
   //     result.push(new Endpoint('toc validation', '/validate/toc', tocDataHMOF));
        result.push(new Endpoint('assignments', '/assignment', assignmentData));
    //    result.push(new Endpoint('assignments validation', '/validate/assignment', assignmentData));
        result.push(new Endpoint('sections', '/sections', { refId: 'b5ad1304-511b-4937-9037-9673dc1b8097' }));
        result.push(new Endpoint('section rooster', '/sectionrosters', { refId: 'd599dcec-4449-44e1-b022-dfaa0070734f' }));
        result.push(new Endpoint('resource association', '/resourceassociation', { sectionRefIds: 'd599dcec-4449-44e1-b022-dfaa0070734f' }));
        result.push(new Endpoint('section annotations', '/annotations', { $filter: '(type_id+eq+9)+and+(content_id+eq+%27d599dcec-4449-44e1-b022-dfaa0070734f%27)', $select: 'annotation_id,type_id,content_id,updated_date,data' }));
    } else if (req.params.environment === 'int' && req.params.platform === 'hmof') {
        result.push(new Endpoint('toc', '/toc', tocDataHMOF));
    //    result.push(new Endpoint('toc validation', '/validate/toc', tocDataHMOF));
        result.push(new Endpoint('assignments', '/assignment', assignmentData));
  //      result.push(new Endpoint('assignments validation', '/validate/assignment', assignmentData));
        result.push(new Endpoint('sections', '/sections', { refId: '2a133c51-816f-4804-abdd-4347cde03dd7' }));
        result.push(new Endpoint('section rooster', '/sectionrosters', { refId: '692b2318-3d70-48f9-9151-8ea8d96ddf40' }));
        result.push(new Endpoint('resource association', '/resourceassociation', { sectionRefIds: '692b2318-3d70-48f9-9151-8ea8d96ddf40' }));
        result.push(new Endpoint('section annotations', '/annotations', { $filter: '(type_id+eq+9)+and+(content_id+eq+%27692b2318-3d70-48f9-9151-8ea8d96ddf40%27)', $select: 'annotation_id,type_id,content_id,updated_date,data' }));
    } else if (req.params.environment === 'cert-review' && req.params.platform === 'tc') {
        result.push(new Endpoint('toc', '/toc', tocDataTc));
  //      result.push(new Endpoint('toc validation', '/validate/toc', tocDataTc));
        result.push(new Endpoint('assignments', '/assignment', assignmentData));
 //       result.push(new Endpoint('assignments validation', '/validate/assignment', assignmentData));
        result.push(new Endpoint('sections', '/sections', { refId: 'a2535f92-9bfa-4655-972b-4fa16f60448b' }));
        result.push(new Endpoint('section rooster', '/sectionrosters', { refId: '7145b29d-ef24-485d-b25d-a9a64c4c9d2f' }));
        result.push(new Endpoint('resource association', '/resourceassociation', { sectionRefIds: '7145b29d-ef24-485d-b25d-a9a64c4c9d2f' }));
        result.push(new Endpoint('section annotations', '/annotations', { $filter: '(type_id+eq+9)+and+(content_id+eq+%277145b29d-ef24-485d-b25d-a9a64c4c9d2f%27)', $select: 'annotation_id,type_id,content_id,updated_date,data' }));
    } else if (req.params.environment === 'int' && req.params.platform === 'tc') {
        result.push(new Endpoint('toc', '/toc', tocDataTc));
  //      result.push(new Endpoint('toc validation', '/validate/toc', tocDataTc));
        result.push(new Endpoint('assignments', '/assignment', assignmentData));
  //      result.push(new Endpoint('assignments validation', '/validate/assignment', assignmentData));
        result.push(new Endpoint('sections', '/sections', { refId: '3e27ad3a-ee09-4bbe-ba23-4f671e8ed155' }));
        result.push(new Endpoint('section rooster', '/sectionrosters', { refId: '87b6a51e-f619-4bb5-a5aa-69cd0707e41d' }));
        result.push(new Endpoint('resource association', '/resourceassociation', { sectionRefIds: '87b6a51e-f619-4bb5-a5aa-69cd0707e41d' }));
        result.push(new Endpoint('section annotations', '/annotations', { $filter: '(type_id+eq+9)+and+(content_id+eq+%2787b6a51e-f619-4bb5-a5aa-69cd0707e41d%27)', $select: 'annotation_id,type_id,content_id,updated_date,data' }));
    } else if (req.params.environment === 'cert' && req.params.platform === 'hmof') {
        result.push(new Endpoint('toc', '/toc', tocDataTc));
     //   result.push(new Endpoint('toc validation', '/validate/toc', tocDataTc));
        result.push(new Endpoint('assignments', '/assignment', assignmentData));
     //   result.push(new Endpoint('assignments validation', '/validate/assignment', assignmentData));
        result.push(new Endpoint('sections', '/sections', { refId: '78de7e3b-87de-4516-a830-fbc945b65dcb' }));
        result.push(new Endpoint('section rooster', '/sectionrosters', { refId: '58a10883-8dda-4d2c-ae10-0fa6e1088423' }));
        result.push(new Endpoint('resource association', '/resourceassociation', { sectionRefIds: '58a10883-8dda-4d2c-ae10-0fa6e1088423' }));
        result.push(new Endpoint('section annotations', '/annotations', { $filter: '(type_id+eq+9)+and+(content_id+eq+%2758a10883-8dda-4d2c-ae10-0fa6e1088423%27)', $select: 'annotation_id,type_id,content_id,updated_date,data' }));
    } else if (req.params.environment === 'cert' && req.params.platform === 'tc') {
        result.push(new Endpoint('toc', '/toc', tocDataTc));
  //      result.push(new Endpoint('toc validation', '/validate/toc', tocDataTc));
        result.push(new Endpoint('assignments', '/assignment', assignmentData));
//        result.push(new Endpoint('assignments validation', '/validate/assignment', assignmentData));
        result.push(new Endpoint('sections', '/sections', { refId: '9bbe2e22-0ef7-4db6-8b34-a3d5fb8143e3' }));
        result.push(new Endpoint('section rooster', '/sectionrosters', { refId: 'a420ffd1-f363-4d79-bdb3-8180659b6876' }));
        result.push(new Endpoint('resource association', '/resourceassociation', { sectionRefIds: 'a420ffd1-f363-4d79-bdb3-8180659b6876' }));
        result.push(new Endpoint('section annotations', '/annotations', { $filter: '(type_id+eq+9)+and+(content_id+eq+%27a420ffd1-f363-4d79-bdb3-8180659b6876%27)', $select: 'annotation_id,type_id,content_id,updated_date,data' }));
    } else { 
        result.push(new Endpoint('toc', '/toc', tocDataHMOF));
   //     result.push(new Endpoint('toc validation', '/validate/toc', tocDataHMOF));
        result.push(new Endpoint('assignments', '/assignment', assignmentData));
 //       result.push(new Endpoint('assignments validation', '/validate/assignment', assignmentData));
        result.push(new Endpoint('sections', '/sections', { refId: 'b5ad1304-511b-4937-9037-9673dc1b8097' }));
        result.push(new Endpoint('section rooster', '/sectionrosters', { refId: 'c1d763f1-fc75-4dc0-b072-e1a06e4f5b6e' }));
        result.push(new Endpoint('resource association', '/resourceassociation', { sectionRefIds: 'c1d763f1-fc75-4dc0-b072-e1a06e4f5b6e' }));
        result.push(new Endpoint('section annotations', '/annotations', { $filter: '(type_id+eq+9)+and+(content_id+eq+%27c1d763f1-fc75-4dc0-b072-e1a06e4f5b6e%27)', $select: 'annotation_id,type_id,content_id,updated_date,data' }));
    }
    
    res.header('Access-Control-Allow-Origin', "*");
    res.send(result);
    next();
};


function optionHandle(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Headers', "token, platform, i, environment");
    res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE");
    res.setHeader('Access-Control-Max-Age', "1728000");
    res.send(200);
    next();
}


function Endpoint(name , dataUrl, params) {
    var self = this;
    self.name = name;
    self.dataUrl = dataUrl;
    self.params = params;
}