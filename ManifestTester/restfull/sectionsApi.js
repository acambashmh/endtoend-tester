﻿var sections = require('./../services/sections.js');
var config = require('../Infrastructure/manifestTesterConfig');
var _ = require('underscore');


exports.addSectionsApiToServer = addApiToServer;

function addApiToServer(server) {
    server.get('/sections', getSections);
    server.opts('/sections', optionHandle);
}


function optionHandle(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Headers', "token, platform, i, environment");
    res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE");
    res.setHeader('Access-Control-Max-Age', "1728000");
    res.send(200);
    next();
}

var getSections = function (req, res, next) {
    sections.getSections(req.headers.token, req.headers.environment, req.params.refId, req.headers.platform, req.params.isStudent).then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        console.log('jbga ' + data);
        res.send(data);
        next();
    });
};
