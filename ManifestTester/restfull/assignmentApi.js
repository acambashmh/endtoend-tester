﻿var assigment = require('./../services/assignment.js');
var assignmentValidatorFactory = require('../Validation/assignmentValidator');
var config = require('../Infrastructure/manifestTesterConfig');

exports.addAssignmentApiToServer = addAssignmentApiToServer;

function addAssignmentApiToServer(server) {
    server.get('/validate/assignment', validateAssignment);
    server.opts('/validate/assignment', optionHandle);
    server.get('/assignment', getAssignment);
    server.opts('/assignment', optionHandle);
    server.get('/assignmentSettings', assingmentSettings);
}


var optionHandle = function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Headers', "token, platform, i, environment");
    res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE");
    res.setHeader('Access-Control-Max-Age', "1728000");
    res.send(200);
    next();
}

var validateAssignment = function (req, res, next) {
    var assignmentValidator = assignmentValidatorFactory.getAssignmentValidator();
    assigment.getAssignments(req.headers.token, req.headers.environment, req.params.sectionId)
    .then(function (data) {
        return assignmentValidator.validate(data);
    })
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
}

var getAssignment = function (req, res, next) {
    assigment.getAssignments(req.headers.token, req.headers.environment, req.params.sectionId)
    .then(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    }).fail(function (data) {
        res.header('Access-Control-Allow-Origin', "*");
        res.send(data);
        next();
    });
};

var assingmentSettings =  function (req, res, next) {
    var result = config.getConfigDataSync().assignmentDefaultValues;
    res.header('Access-Control-Allow-Origin', "*");
    res.send(result);
    next();
}
