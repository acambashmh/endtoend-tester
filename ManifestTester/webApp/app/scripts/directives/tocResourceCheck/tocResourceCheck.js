﻿(function () {
    'use strict';
    var app = angular.module('endPointValidatorUiApp');

    app.directive('tocResource', ['apiServices', function (apiServices) {
        return {
            scope: {
                tocData: '=',
                enviroment: '=',
                platform: '='
            },
            restrict: 'AE',
            replace: 'true',
            templateUrl: 'scripts/directives/tocResourceCheck/tocResourceCheck.html',
            link: function (scope, elem, attrs) {
                scope.enviroments = {
                    int: true,
                    cert_review: true
                };
                scope.checkResourcesResult = null;
                scope.loading = false;
                if (!scope.platform) {
                    scope.platform = 'hmof';
                }
                if (!scope.enviroment) {
                    scope.enviroment = 'cert-review';
                }

                scope.clear = function () {
                    scope.checkResourcesResult = null;
                    scope.loading = false;
                }

                scope.checkResources = function () {
                    scope.clear();
                    scope.loading = true;
                    if (!scope.enviroment && scope.enviroment.length === 0) {
                        alert('you need to pick at least one enviroment');
                        return;
                    }
                    if (!scope.platform && scope.platform.length === 0) {
                        alert('you need to pick one platform');
                        return;
                    }

                    apiServices.checkTocResources(scope.tocData, scope.enviroment, scope.platform).success(function (data) {
                        scope.checkResourcesResult = data;
                        scope.loading = false;
                    }).error(errorHandler);
                }

                function errorHandler(data) {
                    scope.clear();
                    scope.message = 'error checking resources - ' + data;
                }
            }
        };
    }]);

}());