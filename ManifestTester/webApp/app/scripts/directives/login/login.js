﻿(function () {
    'use strict';

    var app = angular.module('endPointValidatorUiApp');
    app.directive('login', ['apiServices', '$routeParams', function (apiServices, $routeParams) {
        return {
            scope: {
                user: '='
            },
            restrict: 'E',
            replace: 'true',
            templateUrl: 'scripts/directives/login/login.html',
            link: function ($scope, elem, attrs) {
                $scope.defaultUsers = [];
                $scope.user = {};


                //init
                apiServices.getConfigDefaultData()
                  .success(function (data) {

                      angular.forEach(data, function (item) {
                          if (!item.platform) {
                              item.platform = 'hmof';
                          }
                      });

                      $scope.defaultUsers = data;
                      $scope.user = data[0];
                      if ($routeParams.username && $routeParams.password &&
                          $routeParams.platform && $routeParams.name) {

                          var user = {
                              username: $routeParams.username,
                              password: $routeParams.password,
                              platform: $routeParams.platform,
                              name: $routeParams.name,
                              displayName: 'custom user for toc',
                          };

                          for (var i = 0; i < $scope.defaultUsers.length; i++) {
                              if ($scope.defaultUsers[i].name === $routeParams.name) {
                                  user.hostUrl = $scope.defaultUsers[i].hostUrl;
                                  user.resourceUrl = $scope.defaultUsers[i].resourceUrl;
                                  break;
                              }
                          }

                          $scope.defaultUsers.push(user);
                          $scope.user = user;

                      }

                  });

            }
        };
    }]);


    function handleUsernameStructure(username, platform) {
        var result = '';
        if (platform === 'tc') {
            var result = 'uid=' + username + ',o=88200010,dc=88200009,st=CA';
        } else {
            result = username;
        }
        return result;
    }


}());