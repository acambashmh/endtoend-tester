﻿(function () {
    'use strict';

    var app = angular.module('endPointValidatorUiApp');
    app.directive('tocView', function () {
        return {
            scope: {
                tocData: '=',
                platformUrl: '=',
                platformUrlTc: '='
            },
            restrict: 'AE',
            replace: 'true',
            templateUrl: 'scripts/directives/tocView/tocView.html',
            link: function (scope, elem, attrs) {
                scope.singleModule = null;
                scope.singleLesson = null;
                scope.selectedUnit = null;
                scope.popupState = false;
                scope.popupContentState = '';

                scope.chooseModule = function (module, unit) {
                    scope.singleModule = module;
                    scope.selectedUnit = unit;
                    scope.popupState = true;
                    scope.popupContentState = popupContentState.lessons;
                }
                scope.closePopup = function () {
                    scope.singleModule = null;
                    scope.popupState = false;
                }

                scope.openLessonsResourcePopup = function (lesson) {
                    scope.singleLesson = lesson;
                    scope.popupContentState = popupContentState.lessonResource;
                }
                scope.closeLessonsResourcePopup = function () {
                    scope.popupContentState = popupContentState.lessons;
                }

                scope.openModuleResourcePopup = function () {
                    scope.popupContentState = popupContentState.moduleResource;
                }
                scope.closeModuleResourcePopup = function () {
                    scope.popupContentState = popupContentState.lessons;
                }

                scope.openUnitResourcePopup = function () {
                    scope.popupContentState = popupContentState.unitResource;
                }
                scope.closeUnitResourcePopup = function () {
                    scope.popupContentState = popupContentState.lessons;
                }

                scope.getDloUrl = function (lesson) {
                    var resourceList = lesson.resources.resource;
                    var resourceListLen = lesson.resources.resource.length;
                    var urlResult = scope.platformUrl;

                    for (var i = 0; i < resourceListLen; i++) {
                        var resource = resourceList[i];

                        var tcUrl = scope.platformUrlTc || resource.uri.platform_url;
                        var hmofUrl = scope.platformUrl || resource.uri.platform_url;

                        if (resource.component_type.toLowerCase() === 'ise'
                            && resource.tool_type === 0
                            //&& resource.assignable
                            && resource.categorization.toLowerCase() === 'core components') {
                            console.log('Type 1');
                            return tcUrl + resource.uri.relative_url;
                        } else if (resource.component_type.toLowerCase() === 'interactive activity'
                            && resource.tool_type === 16
                            && resource.media_type.toLowerCase() === 'html'
                            && resource.assignable) {
                            console.log('Type 2');
                            return hmofUrl + resource.uri.relative_url
                        } else if (resource.component_type.toLowerCase() === 'ise'
                            && resource.tool_type === 16
                            && resource.media_type.toLowerCase() === 'url'
                            && resource.categorization.toLowerCase() === 'core components') {
                            console.log('Type 3');
                            return urlResult + resource.uri.relative_url
                        } else if (resource.component_type.toLowerCase() === 'se'
                            && resource.tool_type === 6
                            && resource.media_type.toLowerCase() === 'url'
                            && resource.categorization.toLowerCase() === 'core components') {
                            console.log('Type 4');
                            return urlResult + resource.uri.relative_url
                        }
                    }

                    return false;
                }

            }
        };
    });


    var popupContentState = {
        moduleResource: 'moduleResource',
        unitResource: 'unitResource',
        lessonResource:'lessonResource',
        lessons:'lessons'
    };



}());