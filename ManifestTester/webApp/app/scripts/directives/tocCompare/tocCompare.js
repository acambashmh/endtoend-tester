﻿(function () {
    'use strict';

    var app = angular.module('endPointValidatorUiApp');

    app.directive('tocCompare', function () {
        return {
            scope: {
                left: '=',
                right: '='
            },
            restrict: 'E',
            replace: 'true',
            templateUrl: 'scripts/directives/tocCompare/tocCompare.html',
            link: function (scope, elem, attrs) {
                var instance = jsondiffpatch.create({
                    objectHash: function (obj) {

                        if (obj.hierarchy) {
                            if (obj.title.indexOf('Solving Equations by Factoring')) {
                                console.log(obj);
                            }

                            var temp = {};
                            temp.hierarchy = obj.hierarchy;
                            temp.type = obj.type;
                            temp.title = obj.title;
                            temp.number = obj.number;

                            return JSON.stringify(temp);
                        } else if (obj.component_type) {
                            var temp = {};
                            temp.id = obj.id;
                            temp.HMH_ID = obj.HMH_ID;
                            temp.display_titl = obj.display_titl;
                            temp.component_type = obj.component_type;
                            temp.media_type = obj.media_type;
                            temp.categorization = obj.categorization;
                            temp.tool_type = obj.tool_type;
                            temp.assignable = obj.assignable;
                            temp.ISBN = obj.ISBN;
                            temp.uri = obj.uri;
                            return JSON.stringify(temp);
                        }

                        return JSON.stringify(obj);
                    }
                });
                var left = scope.left.levels.level[0].level;
                var right = scope.right.levels.level[0].level;

                scope.delta = instance.diff(left, right);

                jsondiffpatch.formatters.html.hideUnchanged();
                $(elem).find('.tocCompare').html(jsondiffpatch.formatters.html.format(scope.delta, left));
                jsondiffpatch.formatters.html.hideUnchanged();
            }
        };
    });





}());