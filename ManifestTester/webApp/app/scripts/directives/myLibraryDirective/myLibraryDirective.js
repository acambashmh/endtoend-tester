(function () {
    'use strict';

    var app = angular.module('endPointValidatorUiApp');
    app.directive('myLibraryDirective', function () {
        return {
            scope: {
                tocData: '='
            },
            restrict: 'AE',
            replace: 'true',
            templateUrl: 'scripts/directives/myLibraryDirective/myLibraryDirective.html',
            link: function (scope, elem, attrs) {

            }
        };
    });




}());