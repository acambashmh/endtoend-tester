(function () {
    'use strict';

    angular
      .module('endPointValidatorUiApp', [
        'jsonFormatter',
        'ngAnimate',
        'ngResource',
        'ngRoute',
        'ui.bootstrap',
        'toastr',
        'endPointValidatorUiApp',
        'angularSpinner',
         'lr.upload'
      ])
      .config(['$routeProvider', function ($routeProvider) {
          $routeProvider
            .when('/', {
                templateUrl: 'scripts/features/checkAll/checkAll.html',
                controller: 'CheckAllCtrl'
            })
            .when('/checkall', {
                templateUrl: 'scripts/features/checkAll/checkAll.html',
                controller: 'CheckAllCtrl'
            })
            .when('/assignmentvalidate', {
                templateUrl: 'scripts/features/assignments/assignmentsValidate.html',
                controller: 'AssignmentValidateCtrl'
            })
            .when('/multiusercheck', {
                templateUrl: 'scripts/features/multiUserCheck/multiusercheck.html',
                controller: 'MultiusercheckCtrl'
            })
            .when('/userdata', {
                templateUrl: 'scripts/features/userData/userdata.html',
                controller: 'UserDataCtrl'
            })
            .when('/toc', {
                templateUrl: 'views/toc.html',
                controller: 'TocCtrl'
            })
            .when('/tocvalidate', {
                templateUrl: 'scripts/features/tocValidate/tocValidate.html',
                controller: 'TocValidateCtrl'
            })
            .when('/generateToc', {
                templateUrl: 'scripts/features/tocGenerate/generateToc.html',
                controller: 'GenerateTocCtrl'
            })
            .when('/toccompare', {
                templateUrl: 'scripts/features/tocPersistence/persistedToc.html',
                controller: 'PersistedTocCtrl'
            })
            .when('/myLibrary', {
                templateUrl: 'scripts/features/myLibrary/myLibrary.html',
                controller: 'MyLibrary'
            })
            .otherwise({
                redirectTo: '/'
            });
      }]);
}());