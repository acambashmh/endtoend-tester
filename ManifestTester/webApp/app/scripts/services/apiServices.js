﻿(function () {
    'use strict';

    var hostUrl = '';
    angular.module('endPointValidatorUiApp')
        .factory('apiServices', ['$http', function ($http) {
            

            var tocSettings = null;

            function login(username, password, environment, platform) {
                var data = {
                    username: username,
                    password: password,
                    environment: environment,
                    platform: platform
                };

                return $http({
                    url: hostUrl + '/login',
                    method: 'POST',
                    data: JSON.stringify(data),
                    headers: { 'Content-Type': 'application/json' }
                });

            };

            function getListOfServices(environment, platform) {

                var data = {
                    environment: environment,
                    platform:platform
                };

                return $http({
                    url: hostUrl + '/getAllServices',
                    method: 'GET',
                    params: data,
                    headers: { 'Content-Type': 'application/json' }
                });

            }
            function getConfigDefaultData() {
                return $http({
                    url: hostUrl + '/getDefaultEnivromentData',
                    method: 'GET',
                });
            }

            function getToc(token, environment, program, grade, non_grade_level, non_grade_title, version, showResources, outputFormat) {
                var params = {
                    program: program,
                    grade: grade,
                    non_grade_level: non_grade_level,
                    non_grade_title: non_grade_title,
                    version: version,
                    showResources: showResources,
                    outputFormat: outputFormat
                };
                var header = {
                    token: token,
                    environment: environment
                };
                return $http({
                    url: hostUrl + '/toc',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }
            function validateToc(token, environment, program, grade, non_grade_level, non_grade_title, version, showResources, outputFormat) {
                var params = {
                    program: program,
                    grade: grade,
                    non_grade_level: non_grade_level,
                    non_grade_title: non_grade_title,
                    version: version,
                    showResources: showResources,
                    outputFormat: outputFormat
                };
                var header = {
                    token: token,
                    environment: environment
                };
                return $http({
                    url: hostUrl + '/validate/toc',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }
            function getTocSettings() {

                return $http({
                    url: hostUrl + '/tocSettings',
                    method: 'GET',
                });
            }

            function getTocList(environment) {

                return $http({
                    url: hostUrl + '/toclist',
                    method: 'GET',
                });
            }


            function getTocFilteredList(program, grade, nonGradeLevel, nonGradeTitle) {
                var params = {
                    program: program,
                    grade: grade,
                    nonGradeLevel: nonGradeLevel,
                    nonGradeTitle: nonGradeTitle,
                };

                return $http({
                    url: hostUrl + '/tocfilteredlist',
                    method: 'GET',
                    params: params
                });
            }


            function checkTocResources(toc, enviroments, platform) {

                var data = {
                    toc: toc,
                    enviroments: enviroments,
                    platform: platform
                };

                return $http({
                    url: hostUrl + '/toccheckresources',
                    method: 'POST',
                    data: data
                });
            }

            function saveToc(toc, environment, program, grade, non_grade_level, non_grade_title, platform) {
                var data = {
                    environment:environment,
                    toc:toc,
                    program: program,
                    grade: grade,
                    non_grade_level: non_grade_level,
                    non_grade_title: non_grade_title,
                    platform: platform
                };

                return $http({
                    url: hostUrl + '/savetoc',
                    method: 'POST',
                    data: data,
                });


            }
            function getTocById(id) {
                var params = {
                    id: id
                };

                return $http({
                    url: hostUrl + '/tocbyid',
                    method: 'GET',
                    params: params,
                });
            }

            function getAssignment(token, environment, sectionId) {
                var params = {
                    sectionId: sectionId
                };
                var header = {
                    token: token,
                    environment: environment,
                };
                return $http({
                    url: hostUrl + '/assignment',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }
            function validateAssignment(token, environment, sectionId) {
                var params = {
                    sectionId: sectionId,
                };
                var header = {
                    token: token,
                    environment: environment
                };
                return $http({
                    url: hostUrl + '/validate/assignment',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }
            function getAssignmentSettings() {
                return $http({
                    url: hostUrl + '/assignmentSettings',
                    method: 'GET',
                });
            }

            function getSections(token, environment, refId, platform, isStudent) {
                var params = {
                    refId: refId,
                    isStudent: isStudent
                };
                var header = {
                    token: token,
                    environment: environment,
                    platform: platform
                };
                return $http({
                    url: hostUrl + '/sections',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }

            function getSectionRosters(token, environment, refId, platform) {
                var params = {
                    refId: refId
                };
                var header = {
                    token: token,
                    environment: environment,
                    platform: platform
                };
                return $http({
                    url: hostUrl + '/sectionrosters',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }

            function getResourceAssociation(token, environment, sectionRefIds, platform) {
                var params = {
                    sectionRefIds: sectionRefIds
                };
                var header = {
                    token: token,
                    environment: environment,
                    platform: platform
                };
                return $http({
                    url: hostUrl + '/resourceassociation',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }

            function getAnnotationsForSection(token, environment, $select, $filter) {
                var params = {
                    $select: $select,
                    $filter: $filter,
                };
                var header = {
                    token: token,
                    environment: environment
                };
                return $http({
                    url: hostUrl + '/annotations',
                    method: 'GET',
                    params: params,
                    headers: header
                });
            }

            function upload(data) {
                return $http({
                    url: hostUrl + '/upload',
                    method: 'POST',
                    data: data,
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                });
            }

            function uploadXmlFile(data) {
                return $http({
                    url: hostUrl + '/uploadXML',
                    method: 'POST',
                    data: data,
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                });
            }


            return {
                getListOfServices: getListOfServices,
                login: login,
                getConfigDefaultData: getConfigDefaultData,
                getToc: getToc,
                validateToc: validateToc,
                getTocSettings: getTocSettings,
                getAssignmentSettings: getAssignmentSettings,
                validateAssignment: validateAssignment,
                getAssignment: getAssignment,
                getSections: getSections,
                getResourceAssociation: getResourceAssociation,
                getSectionRosters: getSectionRosters,
                getAnnotationsForSection: getAnnotationsForSection,
                saveToc: saveToc,
                upload: upload,
                uploadXmlFile: uploadXmlFile,
                getTocList: getTocList,
                getTocById: getTocById,
                checkTocResources: checkTocResources,
                getTocFilteredList: getTocFilteredList
            };

        }]);

}());