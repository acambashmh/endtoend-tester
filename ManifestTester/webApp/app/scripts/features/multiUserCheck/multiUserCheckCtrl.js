﻿(function () {
    'use strict';
    angular.module('endPointValidatorUiApp')
    .controller('MultiusercheckCtrl', ['$scope', '$q', '$http', '$location', 'apiServices',
    function ($scope, $q, $http, $location, apiServices) {

        $scope.defaultAssignmentSettings = {};
        $scope.defaultUsers = [];
        $scope.error = null;
        $scope.loading = false;
        $scope.sectionData = null;
        $scope.token = null;
        $scope.platform = 'hmof';
        $scope.usersDataList = null;


        $scope.getData = function () {
            $scope.clear();
            $scope.loading = true;
            var usersText = $scope.usersText;

            if (!usersText || usersText.length == 0) {
                errorHandler('Text field is empty');
                return
            }
            var usersArray = usersText.split(';');

            if (!usersArray || usersArray.length == 0) {
                errorHandler('The text is wrong format');
                return
            }


            var promisses = [];

            angular.forEach(usersArray, function (value, key) {
                var userData = value.split(':');
                var username = userData[0];
                var password = userData[1];

                var promisse = apiServices.login(handleUsernameStructure(username, $scope.platform),
                    password, $scope.user.name, $scope.platform)
                    .then(function (token) {
                        var deferred = $q.defer();
                        var promise = deferred.promise;
                        var result = {
                            token: token,
                            sections: null
                        };

                        var isStudent = false;
                        if (token.data.roles === 'Learner') {
                            isStudent = true;
                        }
                        apiServices.getSections(token.data.access_token, $scope.user.name, token.data.ref_id, $scope.platform, isStudent)
                            .then(function (data) {
                                result.sections = data;
                                deferred.resolve(result);
                            }, function (data) {
                                var failData = {
                                    user: username,
                                    error:true
                                };

                                deferred.resolve(failData);
                            });

                        return promise;

                    }, errorHandler);

                promisses.push(promisse);
            });

            $q.all(promisses).then(function (data) {
                promisses = [];
                $scope.usersDataList = [];
                angular.forEach(data, function (value, key) {
                    var userDataListItem;

                    if (value.error) {
                        userDataListItem = {
                            username: value.user,
                            error:true
                        };
                    } else {
                        userDataListItem = {
                            username: value.token.data.preferred_username,
                            name: value.token.data.name,
                            roles: value.token.data.roles,
                            sections: value.sections.data.sections
                        };
                    }

                    $scope.usersDataList.push(userDataListItem);
                });
                $scope.loading = false;

            }, function (data) {
                errorHandler(data);
            });

        };

        $scope.clear = function () {
            $scope.usersDataList = null;
            $scope.loading = false;
            $scope.error = null;
        }

        function errorHandler(data) {
            $scope.error = data;
            $scope.usersDataList = null;
            $scope.loading = false;
        }

        //Init
        apiServices.getConfigDefaultData().success(function (data) {
            $scope.defaultUsers = data;
            $scope.user = data[0];
        });

    }]);


    function handleUsernameStructure(username, platform) {
        var result = '';
        if (platform === 'tc') {
            var result = 'uid=' + username + ',o=88200010,dc=88200009,st=CA';
        } else {
            result = username;
        }
        return result;
    }
    function makeAnnotationsParamsForSection(refId) {
        var params = {};
        params.$select = 'annotation_id,type_id,content_id,updated_date,data';
        params.$filter = "(type_id eq 9) and (content_id eq '" + refId + "')";
        return params;
    }

}());