﻿(function () {
    'use strict';

    angular.module('endPointValidatorUiApp')
    .controller('TocValidateCtrl', ['$q', '$scope', '$http', 'apiServices',
         'toastr', '$routeParams',
    function ($q, $scope, $http, apiServices, toastr, $routeParams) {

        $scope.defaultTocSettings = {};
        $scope.user = {};
        $scope.error = null;
        $scope.tocData = null;
        $scope.tocValidationData = null;
        $scope.tocText = null;
        $scope.tocCompareList = null;
        $scope.tocDataToCompare = null;
        $scope.tocCopy = null;

        $scope.$watch('user.username + user.password + user.platform + user.name', function () {
            $scope.clear();
            handleTocUrl();
        });

        $scope.getToc = function () {
            $scope.clear();
            $scope.startSpin();
            //$scope.startSpin();
            //$scope.stopSpin();
            apiServices.login(handleUsernameStructure($scope.user.username, $scope.user.platform), $scope.user.password, $scope.user.name, $scope.user.platform)
                .success(function (token) {
                    var p1 = apiServices.getToc(token.access_token, $scope.user.name, $scope.defaultTocSettings.program, $scope.defaultTocSettings.grade,
                        $scope.defaultTocSettings.non_grade_level, $scope.defaultTocSettings.non_grade_title,
                        $scope.defaultTocSettings.version, $scope.defaultTocSettings.showResources, $scope.defaultTocSettings.outputFormat);
                    var p2 = apiServices.validateToc(token.access_token, $scope.user.name, $scope.defaultTocSettings.program, $scope.defaultTocSettings.grade,
                          $scope.defaultTocSettings.non_grade_level, $scope.defaultTocSettings.non_grade_title,
                          $scope.defaultTocSettings.version, $scope.defaultTocSettings.showResources, $scope.defaultTocSettings.outputFormat);
                    var p3 = apiServices.getTocFilteredList($scope.defaultTocSettings.program, $scope.defaultTocSettings.grade,
                          $scope.defaultTocSettings.non_grade_level, $scope.defaultTocSettings.non_grade_title);
                    $q.all([p1, p2, p3]).then(function (data) {
                        if (data[2].data && data[2].data.length > 0) {
                            $scope.tocCompareList = data[2].data;
                        }
                        $scope.tocText = JSON.stringify(data[0].data);
                        $scope.tocCopy = $.extend(true, {}, data[0].data);
                        $scope.tocValidationData = data[1].data;
                        $scope.tocData = data[0].data;
                        $scope.stopSpin();
                    }, function (data) {
                        $scope.error = data;
                        $scope.stopSpin();
                    });

                }).error(function (data) {
                    $scope.error = data;
                    $scope.stopSpin();
                });


        };
        $scope.clear = function () {
            $scope.error = null;
            $scope.tocData = null;
            $scope.stopSpin();
            $scope.tocValidationData = null;
            $scope.tocCompareList = null;
            $scope.tocDataToCompare = null;
            $scope.tocCopy = null;
        };
        $scope.saveToc = function () {
            if ($scope.tocData !== undefined && $scope.tocData !== null) {


                apiServices.saveToc($scope.tocCopy, $scope.user.name,
                    $scope.defaultTocSettings.program, $scope.defaultTocSettings.grade,
                        $scope.defaultTocSettings.non_grade_level, $scope.defaultTocSettings.non_grade_title
                      )
                .then(function (data) {
                    if (data.data) {
                        toastr.success('Toc save', 'toc data was saves successfuly');
                    }
                });

            }

        };
        $scope.compareTocs = function (compareItem) {
            $scope.startSpin();
            $scope.tocDataToCompare = [];
            $scope.tocDataToCompare.push($scope.tocCopy);

            apiServices.getTocById(compareItem._id).success(function (data) {
                $scope.tocDataToCompare.push(data.toc);
                $scope.stopSpin();
            });
        };

        $scope.tocCompareUrl = function (compareItem) {
            var convertedToc = {
                grade: $scope.defaultTocSettings.grade,
                nonGradeLevel: $scope.defaultTocSettings.non_grade_level,
                nonGradeTitle: $scope.defaultTocSettings.non_grade_title,
                program: $scope.defaultTocSettings.program,
            };
            var queryObject = $.extend({}, {id:compareItem._id}, convertedToc);
            var url = window.location.origin + '/#/toccompare?' + $.param(queryObject);
            return url;
        };

        function handleTocUrl() {
            var convertedToc = {
                grade: $scope.defaultTocSettings.grade,
                nonGradeLevel: $scope.defaultTocSettings.non_grade_level,
                nonGradeTitle: $scope.defaultTocSettings.non_grade_title,
                program: $scope.defaultTocSettings.program,
            };

            var convertedUser = {
                username: $scope.user.username,
                password: $scope.user.password,
                name: $scope.user.name,
                platform: $scope.user.platform,
            };

            var queryObject = $.extend({}, convertedToc, convertedUser);
            var url = window.location.origin + '/#/tocvalidate?' + $.param(queryObject);
            $scope.tocUrl = url;
        }

        //init------------------------------------------------------------------------------------
       

        if (!$routeParams.program) {
            apiServices.getTocSettings()
            .success(function (data) {
                $scope.defaultTocSettings = data;

            });
        } else {
            var convertedTemp = {
                grade: $routeParams.grade.replace(/\+/g, ' '),
                non_grade_level: $routeParams.nonGradeLevel.replace(/\+/g, ' '),
                non_grade_title: $routeParams.nonGradeTitle.replace(/\+/g, ' '),
                program: $routeParams.program.replace(/\+/g, ' '),
                outputFormat: 'json',
                showResources: true,
                version: 'player'
            };
            $scope.defaultTocSettings = convertedTemp;
            setTimeout($scope.getToc, 1500);
        }

    }]);

    function handleUsernameStructure(username, platform) {
        var result = '';
        if (platform === 'tc') {
             result = 'uid=' + username + ',o=88200010,dc=88200009,st=CA';
        } else {
            result = username;
        }
        return result;
    }


}());