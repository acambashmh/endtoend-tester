﻿(function () {
    'use strict';
    angular.module('endPointValidatorUiApp')
    .controller('AssignmentValidateCtrl', ['$scope', '$http', 'apiServices', function ($scope, $http, apiServices) {

        $scope.defaultAssignmentSettings = {};
        $scope.defaultUsers = [];
        $scope.user = {};
        $scope.error = null;
        $scope.assignmentData = null;
        $scope.loading = false;
        $scope.assignmentValidationData = null;
        $scope.platform = 'hmof';
        $scope.getAssignment = function () {
            $scope.clear();
            $scope.loading = true;
            apiServices.login($scope.user.username, $scope.user.password, $scope.user.name, $scope.platform)
                .success(function (token) {
                    apiServices.getAssignment(token.access_token, $scope.user.name, $scope.defaultAssignmentSettings.sectionId)
                      .success(function (data) {
                          $scope.assignmentData = data;
                          $scope.loading = false;
                      })
                      .error(function (data) {
                          $scope.error = data;
                          $scope.assignmentData = null;
                          $scope.loading = false;
                      });

                    apiServices.validateAssignment(token.access_token, $scope.user.name, $scope.defaultAssignmentSettings.sectionId)
                      .success(function (data) {
                          $scope.assignmentValidationData = data;
                          $scope.loading = false;
                      })
                      .error(function (data) {
                          $scope.error = data;
                          $scope.assignmentValidationData = null;
                          $scope.loading = false;
                      });

                }).error(function (data) {
                    $scope.error = data;
                    $scope.loading = false;
                });


        };
        
        $scope.clear = function () {
            $scope.error = null;
            $scope.assignmentData = null;
            $scope.assignmentValidationData = null;
            $scope.loading = false;
        };

        //init
        apiServices.getConfigDefaultData().success(function (data) {
            $scope.defaultUsers = data;
            $scope.user = data[0];
        });

        apiServices.getAssignmentSettings()
            .success(function (data) {
                $scope.defaultAssignmentSettings = data;
            });

    }]);
}());