﻿(function () {
    'use strict';

    angular.module('endPointValidatorUiApp')
    .controller('UserDataCtrl', ['$scope', '$q', '$http', '$location', 'apiServices', 'toastr',
    function ($scope, $q, $http, $location, apiServices, toastr) {

        $scope.defaultAssignmentSettings = {};
        $scope.user = {};
        $scope.error = null;
        $scope.loading = false;
        $scope.sectionData = null;
        $scope.allDataForSection = {};
        $scope.token = null;

        //$scope.startSpin();
        //$scope.stopSpin();

        $scope.getData = function () {
            $scope.clear();
            $scope.startSpin();

            apiServices.login(handleUsernameStructure($scope.user.username, $scope.user.platform), $scope.user.password, $scope.user.name, $scope.user.platform)
                .success(function (token) {
                    var isStudent = false;
                    if (token.roles === 'Learner') {
                        isStudent = true;
                    }
                    
                    $scope.token = {
                        ref_id: token.ref_id,
                        preferred_username: token.preferred_username,
                        name: token.name,
                        roles: token.roles
                    };

                    apiServices.getSections(token.access_token, $scope.user.name, token.ref_id, $scope.user.platform, isStudent)
                        .success(function (data) {
                            $scope.sectionData = data;
                            $scope.stopSpin();
                        })
                        .error(errorHandler);
                })
                .error(errorHandler);
        };


        $scope.getDataForSingleSection = function (section) {
            $scope.allDataForSection = {};
            $scope.startSpin();

            apiServices.login(handleUsernameStructure($scope.user.username, $scope.user.platform), $scope.user.password, $scope.user.name, $scope.user.platform)
                .success(function (token) {
                    var def1 = $q.defer();
                    var p1 = def1.promise;
                    apiServices.getSectionRosters(token.access_token, $scope.user.name, section.refId, $scope.user.platform)
                    .then(function (data) {
                        def1.resolve(data.data);
                    }, function (err) {
                        var data = {};
                        data.error = err.data;
                        def1.reject(data);
                    });

                    var def2 = $q.defer();
                    var p2 = def2.promise;
                    apiServices.getResourceAssociation(token.access_token, $scope.user.name, section.refId, $scope.user.platform)
                    .then(function (data) {
                        def2.resolve(data.data);
                    }, function (err) {
                        var data = {};
                        data.error = err.data;
                        def2.reject(data);
                    });

                    var def3 = $q.defer();
                    var p3 = def3.promise;
                    var annotationParam = makeAnnotationsParamsForSection(section.refId);
                    apiServices.getAnnotationsForSection(token.access_token, $scope.user.name,
                        annotationParam.$select, annotationParam.$filter)
                    .then(function (data) {
                        def3.resolve(data.data);
                    }, function (err) {
                        var data = {};
                        data.error = err.data;
                        def3.reject(data);
                    });

                    var def4 = $q.defer();
                    var p4 = def4.promise;
                    apiServices.getAssignment(token.access_token, $scope.user.name, section.refId)
                    .then(function (data) {
                        def4.resolve(data.data);
                    }, function (err) {
                        var data = {};
                        data.error = err.data;
                        def4.reject(data);
                    });

                    $q.allSettled([p1, p2, p3, p4]).then(function (data) {
                        $scope.allDataForSection.sectionData = section;
                        $scope.allDataForSection.sectionRostersData = data[0];
                        $scope.allDataForSection.resourceData = data[1];
                        $scope.allDataForSection.annotationData = data[2];
                        $scope.allDataForSection.assignmentData = data[3];
                        $scope.stopSpin();
                    },
                        function (data) {
                            $scope.stopSpin();
                            section.error = 'Error getting the data for the section';
                            $scope.allDataForSection.sectionData = section;
                            $scope.allDataForSection.sectionRostersData = data[0];
                            $scope.allDataForSection.resourceData = data[1];
                            $scope.allDataForSection.annotationData = data[2];
                            $scope.allDataForSection.assignmentData = data[3];
                            toastr.error('Error getting the data for the section','Error');
                        }
                    );

                })
                .error(function () {
                    errorHandler('error with login');
                });
        };
        $scope.openDlo = function (tocSearchData) {
            
            var convertedToc = {
                grade: tocSearchData.grade,
                nonGradeLevel: tocSearchData.nonGradeLevel,
                nonGradeTitle: tocSearchData.nonGradeTitle,
                program: tocSearchData.program,
            };

            var convertedUser = {
                username: $scope.user.username,
                password: $scope.user.password,
                name: $scope.user.name,
                platform: $scope.user.platform,
            };

            var queryObject = $.extend({}, convertedToc, convertedUser);
            $location.path('/tocvalidate')
            .search(queryObject)

        };

        $scope.createTocUrl = function (tocSearchData) {
            var convertedToc = {
                grade: tocSearchData.grade,
                nonGradeLevel: tocSearchData.nonGradeLevel,
                nonGradeTitle: tocSearchData.nonGradeTitle,
                program: tocSearchData.program,
            };

            var convertedUser = {
                username: $scope.user.username,
                password: $scope.user.password,
                name: $scope.user.name,
                platform: $scope.user.platform,
            };

            var queryObject = $.extend({}, convertedToc, convertedUser);
            var url = window.location.origin+'/#/tocvalidate?' + $.param(queryObject);
            return url;
        }

        var errorHandler = function (data) {
            $scope.clear();
            $scope.error = data;
        }

        $scope.$watch('user.username + user.password + user.platform + user.name', function () {
            $scope.clear();
        });

        $scope.clear = function () {
            $scope.error = null;
            $scope.stopSpin();
            $scope.sectionData = null;
            $scope.allDataForSection = {};
            $scope.token = null;
        };

    }]);


    function handleUsernameStructure(username, platform) {
        var result = '';
        if (platform === 'tc') {
            var result = 'uid=' + username + ',o=88200010,dc=88200009,st=CA';
        } else {
            result = username;
        }
        return result;
    }
    function makeAnnotationsParamsForSection(refId) {
        var params = {};
        params.$select = 'annotation_id,type_id,content_id,updated_date,data';
        params.$filter = "(type_id eq 9) and (content_id eq '" + refId + "')";
        return params;
    }

}());