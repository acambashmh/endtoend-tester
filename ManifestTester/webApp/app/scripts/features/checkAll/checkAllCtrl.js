﻿(function () {
    'use strict';

    angular.module('endPointValidatorUiApp')
      .controller('CheckAllCtrl', ['$scope', '$http', 'apiServices', function ($scope, $http, apiServices) {
          //data
          $scope.defaultUsers = [];
          $scope.user = {};
          $scope.serviceList = [];
          $scope.errors = [];
          $scope.checkServicesUsed = false;

          $scope.checkServices = function () {
              $scope.checkServicesUsed = true;

              apiServices.login(handleUsernameStructure($scope.user.username, $scope.user.platform), $scope.user.password, $scope.user.name, $scope.user.platform)
              .success(function (data) {
                  angular.forEach($scope.serviceList, function (serviceItem) {
                      serviceItem.dataState = serviceStateEnum.inProgres;
                      (function () {
                          $http({
                              url: serviceItem.dataUrl,
                              method: 'GET',
                              params: serviceItem.params,
                              headers: { token: data.access_token, platform: $scope.user.platform, environment: $scope.user.name },
                          }).success(function (data, a, b, c) {
                              if (data.valid === false) {
                                  serviceItem.dataState = serviceStateEnum.notOk;
                              } else {
                                  serviceItem.dataState = serviceStateEnum.ok;
                              }
                          }).error(function (data, a, b, c) {
                              serviceItem.dataState = serviceStateEnum.notOk;
                              serviceItem.message = data.message;
                          });
                      }(serviceItem))
                  });
              })
              .error(function (data) {
                  var message = 'unknown';
                  if (data && data.message) {
                      message = data.message;
                  }

                  $scope.errors.push('login service error, error details: ' + message);
                  for (var i = 0; i < $scope.serviceList.length; i++) {
                      $scope.serviceList[i].dataState = serviceStateEnum.notOk;
                  }
              });
          };
          $scope.refresh = function () {
              if (!$scope.checkServicesUsed) {
                  return;
              }

              apiServices.getListOfServices($scope.user.name).success(function (data) {
                  $scope.errors = [];
                  $scope.serviceList = [];
                  for (var i = 0; i < data.length; i++) {
                      $scope.serviceList.push(new Endpoint(data[i]));
                  }
                  $scope.checkServicesUsed = false;
              });
          };

          $scope.$watch('user', function () {
              console.log($scope.user.name);
              apiServices.getListOfServices($scope.user.name, $scope.user.platform).success(function (data) {
                  $scope.errors = [];
                  $scope.serviceList = [];
                  for (var i = 0; i < data.length; i++) {
                      $scope.serviceList.push(new Endpoint(data[i]));
                  }
                  $scope.checkServicesUsed = false;
              });

          });

          //Init
          apiServices.getListOfServices($scope.user.name).success(function (data) {
              for (var i = 0; i < data.length; i++) {
                  $scope.serviceList.push(new Endpoint(data[i]));
              }
          });

          apiServices.getConfigDefaultData().success(function (data) {
              $scope.defaultUsers = data;
              $scope.user = data[0];
          });
      }]);


    function Endpoint(data) {
        var self = this;
        self.name = data.name;
        self.dataState = serviceStateEnum.untested;
        self.dataUrl = data.dataUrl;
        self.requestType = data.requestType || 'GET';
        self.params = data.params;
    }

    function handleUsernameStructure(username, platform) {
        var result = '';
        if (platform === 'tc') {
            var result = 'uid=' + username + ',o=88200010,dc=88200009,st=CA';
        } else {
            result = username;
        }
        return result;
    }

    var serviceStateEnum = {
        ok: 'ok',
        notOk: 'notOk',
        inProgres:'inProgres',
        untested: 'untested'
    };

}());