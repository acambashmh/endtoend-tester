﻿(function () {
    'use strict';

    angular.module('endPointValidatorUiApp')
    .controller('PersistedTocCtrl', ['$q', '$scope', '$http', 'apiServices',
        'toastr', '$routeParams',
    function ($q, $scope, $http, apiServices, toastr, $routeParams) {
        $scope.error = null;
        $scope.tocData = null;
        $scope.loading = false;
        $scope.tocCopy = null;
        $scope.tocCompareList = null;
        $scope.standardsUploadData = {};

        $scope.onSuccess = function (response) {
            console.log(response.data);
        }

        $scope.compareTocs = function (compareItem) {
            $scope.startSpin();
            $scope.tocDataToCompare = [];
            $scope.tocDataToCompare.push($scope.tocCopy);

            apiServices.getTocById(compareItem._id).success(function (data) {
                $scope.tocDataToCompare.push(data.toc);
                $scope.stopSpin();
            });
        }

        $scope.tocCompareUrl = function (compareItem) {
            var convertedTemp = {
                grade: $routeParams.grade.replace(/\+/g, " "),
                nonGradeLevel: $routeParams.nonGradeLevel.replace(/\+/g, " "),
                nonGradeTitle: $routeParams.nonGradeTitle.replace(/\+/g, " "),
                program: $routeParams.program.replace(/\+/g, " "),
            };
            var queryObject = $.extend({}, { id: compareItem._id }, $scope.tocParams);
            var url = window.location.origin + '/#/toccompare?' + $.param(queryObject);
            return url;
        };

        //init
        $scope.tocParams = {
            grade: $routeParams.grade.replace(/\+/g, " "),
            nonGradeLevel: $routeParams.nonGradeLevel.replace(/\+/g, " "),
            nonGradeTitle: $routeParams.nonGradeTitle.replace(/\+/g, " "),
            program: $routeParams.program.replace(/\+/g, " "),
        };
        var p1 = apiServices.getTocFilteredList($scope.tocParams.program, $scope.tocParams.grade,
                          $scope.tocParams.nonGradeLevel, $scope.tocParams.nonGradeTitle);

        var p2 = apiServices.getTocById($routeParams.id);

        $q.all([p1, p2]).then(function (data) {
            console.log(data);
            $scope.tocText = JSON.stringify(data[1].data.toc);
            $scope.tocCopy = $.extend(true, {}, data[1].data.toc);
            $scope.tocCompareList = data[0].data;
            $scope.tocData = data[1].data.toc;
            $scope.tocParams.environment = data[1].data.environment;
            $scope.tocParams.platform = 'hmof';
            $scope.tocParams.date = data[1].data.date;
           
            $scope.standardsUploadData = {
                toc: JSON.stringify($scope.tocCopy),
                environment: $scope.tocParams.environment,
                platform : $scope.tocParams.platform
            }
        });

    }]);

}());