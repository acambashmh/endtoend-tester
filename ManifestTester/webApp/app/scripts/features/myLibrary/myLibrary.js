(function () {
    'use strict';
    angular.module('endPointValidatorUiApp')
    .directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                elm.bind('change', function () {
                    $parse(attrs.fileInput)
                    .assign(scope, elm[0].files);
                    scope.$apply();
                });
            }
        };
    }])
    .controller('MyLibrary', ['$scope', '$http', 'apiServices', '$routeParams', function ($scope, $http, apiServices, $routeParams) {
        $scope.toc = null;
        $scope.tocCopy = null;
        $scope.tocCompare = null;
        $scope.tocCompareList = null;
        $scope.tocDataToCompare = null;
        $scope.tocText = null;
        $scope.user = {};
        $scope.platform = 'hmof';
        $scope.enviroment = 'cert-review';
        $scope.uploadObject = {};
        $scope.standardCodeErrors = null;
        var type = ($routeParams.type) ? $routeParams.type : '';

        $scope.uploadWithStandards = function () {
            $scope.startSpin();
            var fd = new FormData();
            fd.append('mds', $scope.uploadObject.mds[0]);
            fd.append('standard', $scope.uploadObject.standard[0]);

            $http.post('/checkSheetManifestStandards', fd,
                {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
            .success(function (data) {
                console.log(data);
                $scope.standardCodeErrors = data;
                $scope.stopSpin();
            }).error(function (e) {
                console.log(e);
                $scope.stopSpin();
            });
        };

        $scope.uploadXmlFile = function () {
            $scope.startSpin();
            var fileData = new FormData();
            fileData.append('file',$scope.uploadObject.mds[0]);
            var upload = null;
            if (type === 'xml') {
                upload = apiServices.uploadXmlFile;
            } else {
                upload = apiServices.upload;
            }
            upload(fileData)
                .success(function (data) {
                    $scope.tocText = JSON.stringify(data);
                    $scope.tocCopy = $.extend(true, {}, data);
                    $scope.toc = data;
                    $scope.stopSpin();
                }).error(function (e) {
                    console.log(e);
                    $scope.stopSpin();
                });
        };

        $scope.compareTocs = function (compareItem) {
            $scope.tocDataToCompare = [];
            $scope.tocDataToCompare.push($scope.tocCopy);

            apiServices.getTocById(compareItem._id)
                .success(function (data) {
                    $scope.tocDataToCompare.push(data.toc);
            });
        };

        $scope.clear = function () {
            $scope.toc = null;
            $scope.tocCopy = null;
            $scope.tocCompare = null;
            $scope.tocCompareList = null;
            $scope.tocDataToCompare = null;
            $scope.tocText = null;
            $scope.standardCodeErrors = null;
        };

        //init----------------------------------------------------------
        apiServices.getTocList().success(function (data) {
            $scope.tocCompareList = data;
        });

        $scope.login = function () {
            var iframe = $('iframe');
            $(iframe).attr('src', 'views/form.html');
            $scope.isForm = true;
            $(iframe).load(function (e) {
                if ($scope.isForm) {
                    $scope.isForm = false;
                    var iframeContent = iframe[0].contentDocument;
                    var submitButton = $(iframeContent).find('input[type="submit"]');
                    var username = $(iframeContent).find('#Username');
                    var password = $(iframeContent).find('#password');
                    var form = $(iframeContent).find('form');
                    $(username).val($scope.user.username);
                    $(password).val($scope.user.password);
                    $(form).attr('action', $scope.user.resourceUrl + '/index.jsp');
                    submitButton.click();
                }
            });
        };

    }]);
})();