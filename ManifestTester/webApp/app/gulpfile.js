﻿var gulp = require('gulp');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
var rimraf = require('gulp-rimraf');


gulp.task('clearBuild', function () {
    return gulp.src('./dist/*', { read: false }) // much faster
    .pipe(rimraf());
});

gulp.task('copyFonts', ['clearBuild'], function () {
    return gulp.src('./bower_components/font-awsome/fonts/*')
    .pipe(gulp.dest('./dist/fonts'))
});

gulp.task('copyHtmlTemp1', ['clearBuild'], function () {
    return gulp.src('./scripts/features/**/*.html', { base: './scripts/features/' })
        //.pipe(minifyHtml({ empty: true }))
        .pipe(gulp.dest('./dist/scripts/features'));
});

gulp.task('copyHtmlTemp2', ['clearBuild'], function () {
    return gulp.src('./scripts/directives/**/*.html', { base: './scripts/directives/' })
         .pipe(minifyHtml({ empty: true }))
        .pipe(gulp.dest('./dist/scripts/directives'));
});

gulp.task('copyHtmlTemp', ['clearBuild', 'copyHtmlTemp1', 'copyHtmlTemp2']);

gulp.task('copyImages', ['clearBuild'], function () {
    return gulp.src('./images/*')
    .pipe(gulp.dest('./dist/images'))
})


gulp.task('usemin', ['copyFonts', 'copyHtmlTemp', 'copyImages'], function () {
    return gulp.src('./index.html')
      .pipe(usemin({
          css: [minifyCss(), 'concat'],
          html: [minifyHtml({ empty: true })],
          js: [uglify(), rev()]
      }))
      .pipe(gulp.dest('./dist/'));
});

gulp.task('default', ['usemin']);